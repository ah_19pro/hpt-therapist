import 'react-native-gesture-handler'
import React, {useEffect} from 'react'
import AppStack from './src/navigation/stack/AuthStack'
import {NavigationContainer} from '@react-navigation/native'
import {SafeAreaProvider} from 'react-native-safe-area-context'
import codePush from 'react-native-code-push'
import {Provider} from 'react-redux'
import store from './src/store/store'
import SplashScreen from 'react-native-splash-screen'

const App = () => {
  useEffect(() => {
    SplashScreen.hide()
  }, [])

  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <NavigationContainer>
          <AppStack />
        </NavigationContainer>
      </Provider>
    </SafeAreaProvider>
  )
}

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_START
}

export default codePush(codePushOptions)(App)
