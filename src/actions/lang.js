import {SET_LANG} from '../types/types'

export const setLang = rtl => {
  return {type: SET_LANG, rtl}
}
