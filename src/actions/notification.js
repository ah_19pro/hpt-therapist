import axios from 'axios'
import {
  SET_FCM_TOKEN,
  SET_NOTIFICATION,
  SET_NOTIFICATION_COUNT
} from '../types/types'
import {refreshList} from './list'
import {showError, strings} from '../common/base/index'
import I18n from 'i18n-js'

export const setFCMToken = fcmToken => {
  return {type: SET_FCM_TOKEN, fcmToken}
}

export const setNotification = data => {
  return {type: SET_NOTIFICATION, data}
}

export const setNotificationCount = data => {
  return {type: SET_NOTIFICATION_COUNT, unReadCount: data}
}

export const getNotificationCount = () => async dispatch => {
  axios
    .get(`notifications`)
    .then(res => {
      dispatch(refreshList(strings.refNotificats))
      dispatch({
        type: SET_NOTIFICATION_COUNT,
        unReadCount: res.data.unreadCount
      })
    })
    .catch(error => {
      if (!error.response) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(I18n.t('ui-error-happened'))
      }
    })
}
