import {SET_USER_DATA, SET_CR_THP_DATA} from '../types/types'

export const setUserData = data => {
  return {type: SET_USER_DATA, data}
}

export const setCrThpData = data => {
  return {type: SET_CR_THP_DATA, data}
}
