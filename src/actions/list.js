import {REFRESH_LIST} from '../types/types'

export const refreshList = list => async dispatch => {
  if (Array.isArray(list)) {
    list.forEach(l => {
      dispatch({type: REFRESH_LIST, list: l})
    })
  } else dispatch({type: REFRESH_LIST, list})
}
