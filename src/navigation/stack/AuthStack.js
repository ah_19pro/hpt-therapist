import {createStackNavigator} from '@react-navigation/stack'
import React from 'react'
import {useSelector} from 'react-redux'
import {routes} from '../../common/defaults/routes'
import CrCompSignUp from '../../screens/auth/centerSignUp/CompSignUp'
import CrModal from '../../screens/auth/centerSignUp/CrModal'
import CrSignUp from '../../screens/auth/centerSignUp/SignUp'
import ForgetPass from '../../screens/auth/forgetPass/ForgetPass'
import Login from '../../screens/auth/login/Login'
import NewPass from '../../screens/auth/newPass/NewPass'
import OTP from '../../screens/auth/otp/OTP'
import SelectAcctType from '../../screens/auth/selectAcctType/SelectAcctType'
import UserTerms from '../../screens/auth/userTerms/userTerms'
import WalkThrough from '../../screens/auth/walkThrough/WalkThrough'
import ShowFile from '../../screens/general/ShowFile'
import Map from '../../screens/map/Map'

const Stack = createStackNavigator()

const AuthStack = () => {
  const userData = useSelector(state => state.user.data)

  return (
    <Stack.Navigator
      initialRouteName={userData ? routes.drawer : routes.walkThrouhg}>
      <Stack.Screen
        name={routes.walkThrouhg}
        component={WalkThrough}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.selectAcctType}
        component={SelectAcctType}
        options={{headerShown: false, gestureEnabled: false}}
      />
      <Stack.Screen
        name={routes.login}
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.forgetPass}
        component={ForgetPass}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.newPass}
        component={NewPass}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.otp}
        component={OTP}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.crModal}
        component={CrModal}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.userTerms}
        component={UserTerms}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.crSignUp}
        component={CrSignUp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.crCompSignUp}
        component={CrCompSignUp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.map}
        component={Map}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={routes.showFile}
        component={ShowFile}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  )
}

export default AuthStack
