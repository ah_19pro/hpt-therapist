export default stringDate => {
  //date format 2020-11-24T17:57:00.159Z
  return stringDate.slice(0, 10) //2020-11-24
}
