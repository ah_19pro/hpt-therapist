import AsyncStorage from '@react-native-community/async-storage'

const USER_DATA_KEY = 'user_data'
const DEVICE_TOKEN_KEY = 'device_token'

/*
USER DATA
*/

export const saveUserData = async data => {
  try {
    await AsyncStorage.setItem(USER_DATA_KEY, JSON.stringify(data))
    console.log('userData is saved now')
  } catch (error) {
    console.log('saveUserData -> error: ' + error)
  }
}

export const readUserData = async () => {
  try {
    const json = await AsyncStorage.getItem(USER_DATA_KEY)
    if (json) {
      console.log('userData', json)
      return JSON.parse(json)
    } else {
      console.log("Reading userData: There's nothing to show")
    }
  } catch (error) {
    console.log('error reading userData: ' + error)
  }
}

export const removeUserData = async () => {
  try {
    await AsyncStorage.removeItem(USER_DATA_KEY)
    console.log('userData removed successfully')
  } catch (error) {
    console.log('userData remove: error: ' + error)
  }
}

/*
Device token
*/
export const saveDeviceToken = async fcmToken => {
  try {
    await AsyncStorage.setItem(DEVICE_TOKEN_KEY, fcmToken)
    console.log('deviceToken is saved now')
  } catch (error) {
    console.log('deviceToken -> error: ', error)
  }
}

export const readDeviceToken = async () => {
  try {
    const fcmToken = await AsyncStorage.getItem(DEVICE_TOKEN_KEY)
    return fcmToken
  } catch (error) {
    console.log('error reading deviceToken:', error)
  }
}

/*
Clear Storage
*/
export const clearStorage = async () => {
  try {
    await AsyncStorage.multiRemove([USER_DATA_KEY, DEVICE_TOKEN_KEY])
    console.log('clear all successfully')
  } catch (error) {
    console.log('clear all: error: ' + error)
  }
}
