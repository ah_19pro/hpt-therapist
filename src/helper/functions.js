/* eslint-disable no-undef */
import I18n from 'i18n-js'
import moment from 'moment'
import DocPicker from 'react-native-document-picker'
import arLocales from '../locales/ar.json'

export function isPHolder(text) {
  if (
    text === I18n.t('country') ||
    text === I18n.t('city') ||
    text === I18n.t('area') ||
    text === I18n.t('Specialities') ||
    text === I18n.t('wDays') ||
    text === I18n.t('wTimes') ||
    text === I18n.t('certificates') ||
    text === I18n.t('from') ||
    text === I18n.t('to') ||
    text === I18n.t('healthSpecialites') ||
    text === I18n.t('insCompany')
  )
    return true
  else return false
}

export async function pickDoc() {
  try {
    const res = await DocPicker.pick({
      type: [DocPicker.types.pdf, DocPicker.types.images]
    })

    return res
  } catch (err) {
    if (DocPicker.isCancel(err)) {
      // User cancelled the picker, exit any dialogs or menus and move on
      console.log('pickDoc -> cancelling', err)
    } else {
      throw err
    }
  }
}

export function upFileParts(file) {
  const data = new FormData()
  if (file) {
    data.append('file', {
      uri: file.uri,
      name: file.name,
      type: file.type
    })
  }
  return data
}

export function isTimeEqualOrBet(time, startTime, endTime) {
  return (
    moment(time, 'HH:mm').isSameOrAfter(moment(startTime, 'HH:mm')) &&
    moment(time, 'HH:mm').isSameOrBefore(moment(endTime, 'HH:mm'))
  )
}

export function getNewCrSchedules(allSchs, crSchs) {
  const newCrSchs = [...allSchs]

  for (let j = 0; j < crSchs.length; j++) {
    newCrSchs.splice(crSchs[j].parent_id - 1, 1, crSchs[j])
  }

  return newCrSchs
}

export function getNewCrThpSchedules(fullThpSchs, thpDays) {
  const array = [...fullThpSchs]

  for (let i = 0; i < thpDays.length; i++) {
    const obj = thpDays[i]
    obj['isCrThp'] = true
    array.splice(thpDays[i].parent_id - 1, 1, obj)
  }

  return array
}

export function convertDateToNormaDay(date) {
  let day
  const isoDay = date.isoWeekday()

  if (isoDay === 6) day = 1
  else if (isoDay === 7) day = 2
  else day = isoDay + 2

  return day
}

export function convertNormalDayToIso(day) {
  let isoDay

  if (day === 1) isoDay = 6
  else if (day === 2) isoDay = 7
  else isoDay = day - 2

  return isoDay
}

export function convertIsoDayToWeekDay(isoDay) {
  if (isoDay === 1) return arLocales.monday
  else if (isoDay === 2) return arLocales.tuesday
  else if (isoDay === 3) return arLocales.wednesday
  else if (isoDay === 4) return arLocales.thursday
  else if (isoDay === 5) return arLocales.friday
  else if (isoDay === 6) return arLocales.saturday
  else if (isoDay === 7) return arLocales.sunday
}

export function getVidExtensionFromUri(uri) {
  const extn = uri.substr(-3).toLowerCase()
  const _extn = uri.substr(-6).toLowerCase()
  if (
    extn === 'mp4' ||
    extn === 'mov' ||
    extn === 'wmv' ||
    extn === 'flv' ||
    extn === 'avi' ||
    extn === 'mkv'
  ) {
    return extn
  } else if (_extn === 'mpeg-4') {
    return _extn
  }

  return null
}
