import {Dimensions} from 'react-native'

const maxWidth = 420
const maxHeight = 800

export const windowWidth = Dimensions.get('screen').width
export const windowHeight = Dimensions.get('screen').height

export const percentWidth = w =>
  `${(Math.min(maxWidth, w) / windowWidth) * 100}%`
export const percentHeight = h =>
  `${(Math.min(maxHeight, h) / windowHeight) * 100}%`

// export const paddingPercent = w => `${100 - (w / windowWidth) * 100}%`
