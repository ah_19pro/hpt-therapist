//auth
export {checkCode} from './auth/checkCode'
export {checkCode2} from './auth/checkCode2'
export {checkUser} from './auth/userCheck'
export {userLogin} from './auth/login'
export {logout} from './auth/logout'
export {sendCode} from './auth/sendCode'
export {checkOTP} from './auth/checkOTP'
export {updatePassword} from './auth/updatePass'
export {getProfile} from './profile/profle'
export {editProfile} from './profile/editProfile'
export {editCrThpProfile} from './profile/editCenterThp'
export {changePassword} from './profile/changePassword'
export {thpCompSignUp} from './auth/thpCompSignUp'
export {centerSignUp} from './auth/centerSignUp'

//countries
export {getCountries} from './countries/countries'
export {getCities} from './countries/cities'
export {getAreas} from './countries/areas'
export {getInsurances} from './insurances/insurances'
export {checkInsurance} from './insurances/insceCheck'
export {getCenters} from './centers/centers'
export {getAllDays} from './schedules/days'
export {getAds} from './ads'
export {getTitles} from './titles/titles'

//specialities
export {getSpecialities} from './specialities/specialities'
export {getCenterSpecs} from './specialities/centerSpecs'

//center images
export {getCenterImgs} from './specialities/centerImages'

//certificates
export {getCertifications} from './certificates/certifcates'
export {getCenteCerts} from './certificates/centerCerticates'

//uploadFile
export {uploadFile} from './uploadFile/uploadFile'
export {uploadPdf} from './uploadFile/uploadPdf'

//prepData
export {prepCrSignUpData} from './data/crSignUp'
export {prepCrCompSignUpData} from './data/crCompSignUp'
export {prepChangePassData} from './data/changePass'

export {CONNECTION_ERROR, GENERAL_ERROR, CANCEL} from './errorTypes'
