import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const deleteCrThp = async id => {
  try {
    const response = await axios.delete(`deleteCenterTherapist/${id}`)
    console.log(
      '🚀 ~ file: deleteCenterTherapist.js ~ line 8 ~ response',
      response
    )
    return response.data
  } catch (error) {
    console.log(
      '🚀 ~ file: deleteCenterTherapist.js ~ line 15 ~ error',
      JSON.stringify(error)
    )
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
