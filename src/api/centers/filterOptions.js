import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const filterData = async data => {
  try {
    const response = await axios.post('filterOptions', data)
    console.log(
      '🚀 ~ file: filterOptions.js ~ line 8 ~ filterData ~ response',
      response
    )
    return response.data
  } catch (error) {
    console.log('🚀 ~ file: filterOptions.js ~ line 15 ~ error', error)
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
