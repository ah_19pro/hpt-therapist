import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const getCenters = async () => {
  try {
    const CancelToken = axios.CancelToken
    const source = CancelToken.source()

    const response = await axios.get('centers', {
      cancelToken: source.token
    })
    return response.data
  } catch (error) {
    if (axios.isCancel(error)) {
      console.log('Request canceled by user', error.message)
    } else if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
