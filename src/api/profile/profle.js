import axios from 'axios'
import I18n from 'i18n-js'
// import {Alert} from 'react-native'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const getProfile = async id => {
  try {
    const CancelToken = axios.CancelToken
    const source = CancelToken.source()

    const response = await axios.get(`profile?user_id=${id}`, {
      cancelToken: source.token
    })
    console.log(
      '🚀 ~ file: getProfile.js ~ line 12 ~ response',
      JSON.stringify(response)
    )
    return response.data
  } catch (error) {
    console.log(
      '🚀 ~ file: getProfile.js ~ line 12 ~ error',
      JSON.stringify(error)
    )
    if (axios.isCancel(error)) {
      console.log('Request canceled by user', error.message)
    } else if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
