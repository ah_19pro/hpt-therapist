import axios from 'axios'
import I18n from 'i18n-js'
// import {Alert} from 'react-native'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const editProfile = async data => {
  try {
    const response = await axios.post('profile', data)
    console.log(
      '🚀 ~ file: editProfile.js ~ line 12 ~ response',
      JSON.stringify(response)
    )
    return response.data
  } catch (error) {
    console.log(
      '🚀 ~ file: editProfile.js ~ line 12 ~ error',
      JSON.stringify(error)
    )
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
