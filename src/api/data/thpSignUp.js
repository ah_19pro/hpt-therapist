import moment from 'moment'
import {Alert} from 'react-native'
import {strings} from '../../common/base'

export const prepThpSignUpData = values => {
  if (values) {
    return {
      user_type: values.userType ? strings.teleThp : strings.therap,
      first_name: values.firstName,
      last_name: values.lastName,
      email: values.email,
      title_id: values.titleId,
      mobile: values.phoneNo,
      password: values.password,
      password_confirmation: values.passConfirm,
      age: values.age,
      commission: values.commHealthSpecNo,
      country_id: values.ctryId,
      city_id: values.cityId,
      area_id: values.areaId,
      hour_rate: values.hrRate,
      certificates: values.certs,
      courses: values.courses,
      organization: values.organizs,
      specialization: values.specials,
      therapist_availabillity: values.teleThp,
      schedule_days: values.wDays,
      from_time: values.wTimeFrom,
      to_time: values.wTimeTo,
      lat: values.location.latitude,
      lng: values.location.longitude
    }
  }
}
