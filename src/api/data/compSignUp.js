export const prepCompSignUpData = values => {
  if (values)
    return {
      first_exmination_fees: values.medCost,
      seven_sessions: values.pack7SesPr,
      twelve_sessions: values.pack12SesPr
    }
}
