export const prepCrCompSignUpData = values => {
  if (values) {
    const _wTFromValue = values.wTimeFrom
    const _wTToValue = values.wTimeTo
    const _fTimesValue = values.fromTimes
    const _tTimesValue = values.toTimes

    const _values = {
      first_exmination_fees: values.medCost,
      seven_sessions: values.pack7SesPr,
      twelve_sessions: values.pack12SesPr,
      avatar: values.avatar,
      first_name: values.firstName,
      last_name: values.lastName,
      age: values.age,
      specialization: values.specs,
      certificates: values.certs,
      courses: values.courses,
      organization: values.organizs,
      schedule_days: values.wDays
    }

    if (_wTFromValue && _wTToValue) {
      _values['from_time'] = _wTFromValue
      _values['to_time'] = _wTToValue
    } else if (_fTimesValue && _tTimesValue) {
      _values['from_times'] = _fTimesValue
      _values['to_times'] = _tTimesValue
    }

    return _values
  }
}
