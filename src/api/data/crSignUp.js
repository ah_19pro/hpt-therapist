import moment from 'moment'

export const prepCrSignUpData = values => {
  if (values)
    return {
      user_type: 'center',
      branch: values.branchId,
      branch_id: values.branchId ? values.mainBrId : '',
      center_name: values.crName,
      description: values.aboutCR,
      email: values.email,
      mobile: values.phoneNo,
      password: values.password,
      password_confirmation: values.passConfirm,
      commission: values.comHSpecNo,
      country_id: values.ctryId,
      city_id: values.cityId,
      area_id: values.areaId,
      specialization: values.specs,
      schedule_days: values.wDays,
      from_time: values.wTimeFrom,
      to_time: values.wTimeTo,
      commercial_registration: values.comReg,
      commercial_name: values.commerNm,
      images: values.crImages,
      insurance_companies: values.insces,
      insurance_company: values.insce,
      lat: values.location.latitude,
      lng: values.location.longitude
    }
}
