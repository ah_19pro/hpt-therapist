export const prepChangePassData = values => {
  if (values)
    return {
      old_password: values.oldPass,
      new_password: values.newPass,
      confirmation_password: values.passConfirm
    }
}
