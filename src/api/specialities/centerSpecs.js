import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const getCenterSpecs = async (id, value = 'asc') => {
  try {
    const response = await axios.get(`centerSpecialties?id=${id}&sort=${value}`)
    console.log(
      '🚀 ~ file: centerSpecs.js ~ line 8 ~ getCenterSpecs ~ response',
      response
    )
    return response.data
  } catch (error) {
    console.log(
      '🚀 ~ file: centerSpecs.js ~ line 12 ~ getCenterSpecs ~ error',
      JSON.stringify(error)
    )
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
