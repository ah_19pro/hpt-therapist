import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const getCenterImgs = async (id, value = 'desc') => {
  try {
    const response = await axios.get(`centerImages?id=${id}&sort=${value}`)
    console.log(
      '🚀 ~ file: getCenterImgs.js ~ line 8 ~ getCenterImgs ~ response',
      response
    )
    return response.data
  } catch (error) {
    console.log(
      '🚀 ~ file: getCenterImgs.js ~ line 12 ~ getCenterImgs ~ error',
      JSON.stringify(error)
    )
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
