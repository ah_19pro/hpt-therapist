import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const uploadFile = async data => {
  try {
    const response = await axios.post('uploadFile', data)
    return response.data
  } catch (error) {
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.errors
      }
    }
  }
}
