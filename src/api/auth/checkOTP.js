import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const checkOTP = async data => {
  try {
    const response = await axios.post('checkOTP', data)
    return response.data
  } catch (error) {
    console.log('🚀 ~ file: checkOTP.js ~ line 11 ~ error', error)
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
