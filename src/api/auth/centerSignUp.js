import axios from 'axios'
import I18n from 'i18n-js'
import {Alert} from 'react-native'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const centerSignUp = async data => {
  try {
    const response = await axios.post('center/register', data)
    console.log('🚀 ~ file: centerSignUp.js ~ line 9 ~ response', response)
    return response.data
  } catch (error) {
    // Alert.alert('centerSignUp', JSON.stringify(error))
    console.log('centerSignUp - error', JSON.stringify(error))
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
