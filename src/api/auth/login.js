import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const userLogin = async data => {
  try {
    const response = await axios.post('login', data)
    console.log('🚀 ~ file: login.js ~ line 8 ~ response', response)
    return response.data
  } catch (error) {
    if (!error.response) {
      // console.log('🚀 ~ file: userLogin.js ~ line 12 ~ error', error)
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
