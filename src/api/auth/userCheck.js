import axios from 'axios'
import I18n from 'i18n-js'
import {CONNECTION_ERROR, GENERAL_ERROR} from '../errorTypes'

export const checkUser = async (key, value, user_type) => {
  try {
    const response = await axios.get(
      `userCheck?key=${key}&value=${value}&user_type=${user_type}`
    )
    return response.data
  } catch (error) {
    console.log(
      '🚀 ~ file: checkCode.js ~ line 11 ~ error',
      JSON.stringify(error)
    )
    if (!error.response) {
      throw {
        type: CONNECTION_ERROR,
        message: I18n.t('ui-networkConnectionError')
      }
    } else {
      throw {
        type: GENERAL_ERROR,
        message: error.response.data.msg
      }
    }
  }
}
