import axios from 'axios'
import * as urls from './urls'
import store from '../store/store'

export default () => {
  // Add a request interceptor
  axios.interceptors.request.use(
    config => {
      config.baseURL = urls.BASE_URL
      return {
        ...config,
        headers: {
          ...config.headers,
          'Accept-Language': 'ar',
          Authorization: store.getState().user.data
            ? store.getState().user.data.token
            : null
        }
      }
    },
    error => {
      // TODO custom error interseptor
      return Promise.reject(error)
    }
  )
}
