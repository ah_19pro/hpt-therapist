export const SET_LANG = 'SET_LANG'
export const SET_USER_DATA = 'SET_USER_DATA'
export const SET_CR_THP_DATA = 'SET_CR_THP_DATA'
export const REFRESH_LIST = 'REFRESH_LIST'

//notification
export const SET_FCM_TOKEN = 'SET_FCM_TOKEN'
export const SET_NOTIFICATION = 'SET_NOTIFICATION'
export const SET_NOTIFICATION_COUNT = 'SET_NOTIFICATION_COUNT'
