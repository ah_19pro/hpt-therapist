import I18n from 'i18n-js'
import arLocales from '../locales/ar.json'
import enLocales from '../locales/en.json'
import defAr from '../common/defaults/ar.json'
import defEn from '../common/defaults/en.json'
import {I18nManager} from 'react-native'
import configRestApi from '../api/config'
import {readUserData} from '../helper/localStorage'
import store from '../store/store'
import {setUserData} from '../actions/user'
import {setLang} from '../actions/lang'

export const configApp = async () => {
  configLang()

  await getUserData()

  configRestApi()
}

const configLang = () => {
  I18nManager.allowRTL(false)
  I18nManager.forceRTL(false)

  I18n.fallbacks = true
  I18n.translations = {
    ar: {...arLocales, ...defAr},
    en: {...enLocales, ...defEn}
  }

  store.dispatch(setLang(true))

  I18n.locale = 'ar'
}

const getUserData = async () => {
  const userData = await readUserData()
  if (userData) {
    store.dispatch(setUserData(userData))
  }
}
