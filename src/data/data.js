// import I18n from 'i18n-js'
import I18n from 'i18n-js'
import arLocales from '../locales/ar.json'

export const confirms = [
  {id: 0, name: arLocales.no},
  {
    id: 1,
    name: arLocales.yes
  }
]

// export const sortAlpha = [
//   {id: 0, name: `${I18n.t('sortAlpha')} ${I18n.t('A-Z')}`}
// ]

// export const sortRateFees = [
//   {
//     id: 0,
//     name: `${I18n.t('sort')} ${I18n.t('theRate')} ${I18n.t('fromHighToLow')}`
//   },
//   {
//     id: 1,
//     name: `${I18n.t('sort')} ${I18n.t('theRate')} ${I18n.t('fromLowToHight')}`
//   },
//   {
//     id: 2,
//     name: `${I18n.t('sort')} ${I18n.t('medCost')} ${I18n.t('fromHighToLow')}`
//   },
//   {
//     id: 3,
//     name: `${I18n.t('sort')} ${I18n.t('medCost')} ${I18n.t('fromLowToHight')}`
//   }
// ]

export const sortAlphabet = [
  {
    id: 0,
    name: `${arLocales.sortAlpha} ${arLocales.from} ${arLocales.AtoZ}`,
    type: 'asc'
  },
  {
    id: 1,
    name: `${arLocales.sortAlpha} ${arLocales.from} ${arLocales.ZtoA}`,
    type: 'desc'
  }
]

export const sortByDate = [
  {
    id: 0,
    name: `${arLocales.sort} ${arLocales.from} ${arLocales.newToOld}`,
    type: 'desc'
  },
  {
    id: 1,
    name: `${arLocales.sort} ${arLocales.from} ${arLocales.oldToNew}`,
    type: 'asc'
  }
]

export const sortRateFees = [
  {
    id: 0,
    name: `${arLocales.highRate}`,
    type: {key: 'rate', value: 'desc'}
  },
  {
    id: 1,
    name: `${arLocales.sort} ${arLocales.medCost} ${arLocales.highToLow}`,
    type: {key: 'examFees', value: 'desc'}
  },
  {
    id: 2,
    name: `${arLocales.sort} ${arLocales.medCost} ${arLocales.lowToHight}`,
    type: {key: 'examFees', value: 'asc'}
  }
]

export const sortRateDate = [
  {
    id: 0,
    name: `${arLocales.sort} ${arLocales.theRates} ${arLocales.from} ${arLocales.highToLow}`,
    type: {key: 'rate', value: 'desc'}
  },
  {
    id: 1,
    name: `${arLocales.sort} ${arLocales.theRates} ${arLocales.from} ${arLocales.lowToHight}`,
    type: {key: 'rate', value: 'asc'}
  },
  {
    id: 2,
    name: `${arLocales.sort} ${arLocales.from} ${arLocales.newToOld}`,
    type: 'desc'
  },
  {
    id: 3,
    name: `${arLocales.sort} ${arLocales.from} ${arLocales.oldToNew}`,
    type: 'asc'
  }
]

//day times

export const emptyDTsAr = [
  {
    name: arLocales.saturday,
    from_time: null,
    to_time: null,
    parent_id: 1
  },
  {
    name: arLocales.sunday,
    from_time: null,
    to_time: null,
    parent_id: 2
  },
  {
    name: arLocales.monday,
    from_time: null,
    to_time: null,
    parent_id: 3
  },
  {
    name: arLocales.tuesday,
    from_time: null,
    to_time: null,
    parent_id: 4
  },
  {
    name: arLocales.wednesday,
    from_time: null,
    to_time: null,
    parent_id: 5
  },
  {
    name: arLocales.thursday,
    from_time: null,
    to_time: null,
    parent_id: 6
  },
  {
    name: arLocales.friday,
    from_time: null,
    to_time: null,
    parent_id: 7
  }
]
