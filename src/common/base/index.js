export {default as AppCheckBx} from '../CheckBox'
export {default as AppButton} from '../button'
export {default as DTPicker} from '../DTPicker'
export {default as AppHeader} from '../Header'
export {default as AppIcon} from '../Icon'
export {default as AppImage} from '../Image'
export {default as AppIndicator} from '../Indicator'
export {default as AppInput} from '../Input'
export {default as KBAvoidView} from '../KBAvoidView'
export {default as AppList} from '../List'
export {default as AppSectionList} from '../SectionList'
export {default as AppPicker} from '../Picker'
export {default as RB} from '../RB'
export {default as SafeArea} from '../SafeArea'
export {default as AppScrollView} from '../ScrollView'
export {default as SysPicker} from '../SysPicker'
export {default as AppText} from '../Text'
export {default as ValidError} from '../ValidError'
export {default as AppView} from '../View'
export {default as AppCard} from '../Card'
export {default as AppRating} from '../Rating'
export {default as AppRefresh} from '../RefreshControl'
export {default as AppStatusBar} from '../FocusAwareStatusBar'
export {default as AppSpinner} from '../Spinner'
export {default as PdfViewer} from '../PdfViewer'
export {default as AppModal} from '../Modal'
export {default as AppAlert} from '../Alert'

export {
  showError,
  showInfo,
  showSuccess
} from '../../common/utils/localNotifications'
export {
  responsiveHeight,
  responsiveWidth,
  moderateScale
} from '../../common/utils/responsiveDimensions'

export {default as arLocales} from '../../common/defaults/ar.json'
export {default as enLocales} from '../../common/defaults/en.json'
export {colors} from '../../common/defaults/colors'
export {fonts} from '../../common/defaults/fonts'
export {images} from '../../common/defaults/images'
export {routes} from '../../common/defaults/routes'
export {strings} from '../../common/defaults/strings'
export {defStyles} from '../../common/defaults/styles'
export {theme} from '../../common/defaults/theme'
export {iconsType} from '../../common/utils/icons'
export {defIcons} from '../../common/defaults/icons'
export {defSvg} from '../../common/defaults/svg'
