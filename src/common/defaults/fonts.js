import {Platform} from 'react-native'

export const fonts = {
  normal: Platform.OS === 'android' ? 'Hacen-Maghreb-Lt' : 'Hacen Maghreb Lt',
  bold: Platform.OS === 'android' ? 'Hacen-Maghreb-Bd' : 'Hacen Maghreb Bd'
  // boldIsStyle: true
}
