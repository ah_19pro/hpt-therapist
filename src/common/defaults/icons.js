/* eslint-disable react/display-name */
import React from 'react'
import {AppIcon, colors, iconsType, moderateScale} from '../base/index'
import Icon from '../Icon'

export const defIcons = {
  checkedBx: (
    <Icon
      type={iconsType.materialComm}
      name={'checkbox-intermediate'}
      color={colors.secondary.color}
    />
  ),
  uncheckedBx: color => (
    <Icon
      type={iconsType.materialComm}
      name={'checkbox-blank-outline'}
      color={color || colors.lightGrey3.color}
    />
  ),
  eyeOn: (
    <AppIcon
      type={iconsType.ion}
      name={'ios-eye-outline'}
      color={colors.darkGrey.color}
      size={moderateScale(9)}
    />
  ),
  eyeOff: (
    <AppIcon
      type={iconsType.ion}
      name={'ios-eye-off'}
      color={colors.lightGrey.color}
      size={moderateScale(9)}
    />
  ),
  arrowLeft: (
    <AppIcon
      type={iconsType.material}
      name={'keyboard-arrow-left'}
      size={moderateScale(7)}
    />
  ),
  checkCircle: (
    <AppIcon
      type={iconsType.ion}
      name={'checkmark-circle-outline'}
      size={moderateScale(11)}
      color={colors.darkGreen.color}
    />
  ),
  dots: (
    <AppIcon
      type={iconsType.entypo}
      name={'dots-three-vertical'}
      color={colors.lightGrey3.color}
      size={moderateScale(9)}
    />
  ),
  dollar: (
    <AppIcon
      type={iconsType.fontAwesome}
      name={'dollar'}
      color={colors.lightGrey3.color}
      size={moderateScale(8)}
    />
  )
}
