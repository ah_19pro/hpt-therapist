export const images = {
  logo: require('../../assets/images/png/logo.png'),
  therapist: require('../../assets/images/png/Therapist.png'),
  intImg1: require('../../assets/images/png/introImg1.png'),
  intImg2: require('../../assets/images/png/introImg2.png'),
  intImg3: require('../../assets/images/png/introImg3.png'),
  avatar: require('../../assets/images/png/avatar.png'),
  bones: require('../../assets/images/png/bones.png'),
  menuBgrd: require('../../assets/images/png/menuBgrd.png')
}
