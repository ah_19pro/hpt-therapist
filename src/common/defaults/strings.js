export const strings = {
  therap: 'therapist',
  center: 'center',
  teleThp: 'tele-therapist',
  centertherapist: 'centertherapist',
  refArticles: 'refreshArticles',
  refCrThps: 'refreshCenterThps',
  refReservs: 'refreshReservations',
  refNotificats: 'refreshNotifications'
}
