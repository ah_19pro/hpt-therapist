import React from 'react'
import {responsiveHeight, colors} from '../base'
import ArrowDown from '../../assets/images/svg/arrowDown.svg'
import Badge from '../../assets/images/svg/badge.svg'
import Money from '../../assets/images/svg/money.svg'

export const defSvg = {
  arrowDown: (
    <ArrowDown
      height={responsiveHeight(2)}
      width={responsiveHeight(2)}
      color={colors.darkGrey.color}
    />
  ),
  badge: <Badge height={responsiveHeight(4)} width={responsiveHeight(4)} />,
  money: <Money height={responsiveHeight(4)} width={responsiveHeight(4)} />
}
