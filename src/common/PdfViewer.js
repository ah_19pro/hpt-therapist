/* eslint-disable react/prop-types */
import React from 'react'
import Pdf from 'react-native-pdf'
import {AppSpinner, AppView} from './base/index'

const PdfReader = props => {
  const {ctStyle, pdfCtStyle, uri, onClose, onLoadComplete, loading} = props

  return (
    <AppView style={ctStyle}>
      <Pdf
        style={pdfCtStyle}
        source={{uri}}
        onPageSingleTap={onClose}
        {...{onLoadComplete}}
        activityIndicator={<AppSpinner {...{loading}} />}
      />
    </AppView>
  )
}

export default PdfReader
