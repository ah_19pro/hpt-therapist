/* eslint-disable react/prop-types */
import React, {useEffect} from 'react'
import {Alert} from 'react-native'
import {ActivityIndicator} from 'react-native'
import {Image} from 'react-native-elements'
import {AppIndicator, AppView} from './base'
import {defStyles} from './defaults/styles'
import ValidError from './ValidError'

const AppImage = props => {
  const {
    ctStyle,
    imgStyle,
    source,
    loading,
    pHolderStyle,
    onPress,
    resizeMode,
    stretch,
    fullHeight,
    alignCr,
    schKey,
    setFieldValue,
    error,
    touched
  } = props

  const _ctStyle = [
    alignCr ? defStyles.alignCenter : {},
    stretch ? defStyles.stretch : {},
    fullHeight ? defStyles.fullHeight : {},
    ctStyle
  ]

  const uri = source?.uri

  useEffect(() => {
    if (schKey && uri) {
      setFieldValue(schKey, uri)
    }
  }, [uri])

  return (
    <AppView>
      {loading ? (
        <AppView center style={_ctStyle}>
          <AppIndicator {...{loading}} />
        </AppView>
      ) : (
        <Image
          containerStyle={_ctStyle}
          style={imgStyle}
          {...{source}}
          placeholderStyle={pHolderStyle}
          resizeMode={resizeMode}
          onPress={onPress}
        />
      )}

      {error && touched ? <ValidError {...{error}} /> : null}
    </AppView>
  )
}

export default AppImage
