/* eslint-disable react/prop-types */
import moment from 'moment'
import React, {memo, useCallback, useEffect} from 'react'
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import Mandatory from '../components/thpSignUp/Mandatory'
import {AppText, AppView} from './base/index'
import Picker from './Picker'

const DTPicker = props => {
  const {
    style,
    tCtStyle,
    pickerCt,
    pickCdStyle,
    tStyle,
    iconColor,
    schKey,
    isVisible,
    type = 'time',
    cpTitle,
    mandTitle,
    title,
    data = '',
    value,
    touched,
    error,
    setFieldValue,
    setFieldTouched,
    onCancel,
    onPress
  } = props

  useEffect(() => {
    if (value && schKey) {
      setFieldValue(schKey, value)
    }
  }, [value])

  const hidePicker = useCallback(() => {
    onCancel()
  }, [onCancel])

  const handleConfirm = useCallback(
    date => {
      if (schKey) setFieldValue(schKey, getDateFormat(date))
      if (setFieldTouched) setFieldTouched(schKey, true)
      hidePicker()
    },
    [schKey, onCancel, hidePicker]
  )

  const getDateFormat = date => {
    return `${moment(date).locale('en').format('HH')}:00:00`
    // `${moment(date).locale('en').format('YYYY-MM-DD')}T${moment(date)
    //   .locale('en')
    //   .format('HH')}:00:00`
  }

  return (
    <AppView style={tCtStyle}>
      {cpTitle ? <AppText>{cpTitle}</AppText> : null}
      {mandTitle ? <Mandatory /> : null}
      <Picker
        ct={pickerCt}
        {...{style}}
        {...{cardStyle: pickCdStyle}}
        {...{color: iconColor}}
        {...{tStyle}}
        {...{data}}
        {...{title}}
        {...{onPress}}
        {...{error}}
        {...{touched}}
      />
      <DateTimePickerModal
        isVisible={isVisible}
        mode={type}
        onConfirm={handleConfirm}
        onCancel={hidePicker}
        minimumDate={new Date(Date.now())}
      />
    </AppView>
  )
}

function areEqual(prevProps, nextProps) {
  return (
    prevProps.title === nextProps.title &&
    prevProps.isVisible === nextProps.isVisible
  )
}

export default memo(DTPicker, areEqual)
