/* eslint-disable react/prop-types */
import React, {memo, useEffect, useState} from 'react'
import {
  AppCard,
  AppIcon,
  AppIndicator,
  AppText,
  AppView,
  colors,
  defStyles,
  iconsType,
  theme,
  ValidError
} from '../common/base/index'
import Mandatory from '../components/thpSignUp/Mandatory'
import {isPHolder} from '../helper/functions'

const Picker = props => {
  const {
    style,
    titleStyle,
    ct = defStyles.bot3,
    ctStyle = theme.picker,
    cardStyle = theme.card,
    type = iconsType.material,
    name = 'keyboard-arrow-down',
    color = colors.darkGrey.color,
    title,
    mandTitle,
    onPress,
    loading,
    tStyle = theme.tInpStyle,
    data = [],
    schKey,
    setFieldValue,
    error,
    touched,
    ...rest
  } = props

  const _tStyle = isPHolder(title) ? theme.pHolderStyle : tStyle

  const _ctStyle = [ctStyle, style]

  const [oneText, setOneText] = useState('')

  useEffect(() => {
    if (data && schKey) {
      handleIds()
    }
    if (data?.length > 0) {
      handleText()
    }
  }, [data?.length])

  const handleIds = () => {
    const arry = []
    if (data.length > 0) {
      data.forEach(item => {
        const {parent_id, id} = item
        arry.push(parent_id || id)
      })
    }
    setFieldValue(schKey, arry)
  }

  const handleText = () => {
    let text = ''
    const length = data.length

    for (let index = 0; index < length; index++) {
      const name = data[index].name
      text += index < length - 1 ? `${name}, ` : name
    }

    setOneText(text)
  }

  return (
    <AppView style={ct}>
      {mandTitle ? <Mandatory /> : null}
      <AppCard cardStyle={cardStyle}>
        <AppView
          row
          touchableView
          style={_ctStyle}
          {...{...rest}}
          onPress={onPress}>
          <AppIcon type={type} name={name} color={color} />
          {loading ? (
            <AppView grow center>
              <AppIndicator loading={loading} />
            </AppView>
          ) : data.length > 0 ? (
            <AppView colEnd style={defStyles.width85}>
              <AppText numberOfLines={1}>{oneText}</AppText>
            </AppView>
          ) : (
            <AppView style={titleStyle}>
              <AppText tStyle={_tStyle} numberOfLines={1}>
                {title}
              </AppText>
            </AppView>
          )}
        </AppView>
      </AppCard>
      {error && touched ? <ValidError {...{error}} /> : null}
    </AppView>
  )
}

function areEqual(prevProps, nextProps) {
  return (
    prevProps.data?.length === nextProps.data?.length &&
    prevProps.title === nextProps.title &&
    prevProps.loading === nextProps.loading &&
    prevProps.error === nextProps.error &&
    prevProps.touched === nextProps.touched
  )
}

export default memo(Picker, areEqual)
