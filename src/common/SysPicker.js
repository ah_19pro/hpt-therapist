/* eslint-disable react/prop-types */
import React, {memo, useCallback, useEffect, useState} from 'react'
import {Alert} from 'react-native'
import {StyleSheet} from 'react-native'
import Picker from 'react-native-picker-select'
import Mandatory from '../components/thpSignUp/Mandatory'
import {
  AppCard,
  AppIcon,
  AppIndicator,
  AppView,
  colors,
  defStyles,
  iconsType,
  theme,
  ValidError
} from './base/index'

const SysPicker = ({
  topCtStyle = defStyles.bot2,
  ctStyle = theme.picker,
  cardStyle = theme.card,
  type = iconsType.material,
  name = 'keyboard-arrow-down',
  picked,
  onPick,
  title,
  value,
  schKey,
  _key,
  setFieldValue,
  setFieldTouched,
  data,
  hint = '',
  loading,
  touched,
  error
}) => {
  const [items, setItems] = useState([])

  const placeholder = {
    label: hint,
    value: null,
    color: colors.lightGrey.color
  }

  useEffect(() => {
    if (data) {
      setItems(
        data.map(item => ({
          value: item.id,
          label: item.name
        }))
      )
    }
  }, [data])

  useEffect(() => {
    if (picked) {
      setFieldValue(schKey, picked)
    }
  }, [picked])

  useEffect(() => {
    if (_key) setFieldValue(schKey, picked)
  }, [picked])

  const handleChange = useCallback(
    value => {
      if (onPick) onPick(value)
      if (schKey) setFieldValue(schKey, value !== null ? value : '')
    },
    [setFieldValue, schKey, onPick]
  )

  const handlePress = useCallback(() => {
    setFieldTouched(schKey, true)
  }, [setFieldTouched, schKey])

  const handleIcon = useCallback(() => {
    return null
  }, [])

  return (
    <AppView stretch style={topCtStyle}>
      {title ? <Mandatory /> : null}
      <AppCard cardStyle={cardStyle}>
        <AppView row style={ctStyle}>
          <AppIcon type={type} name={name} />
          <AppView grow fullHeight centerY>
            {loading ? (
              <AppIndicator loading={loading} />
            ) : (
              <Picker
                onOpen={handlePress}
                style={pickerSelectStyles}
                // textInputProps={{numberOfLines: 2, multiline: true}}
                onValueChange={handleChange}
                Icon={handleIcon}
                items={items}
                value={value}
                placeholder={placeholder}
                touchableWrapperProps={{backgroundColor: 'white'}}
                pickerProps={{backgroundColor: 'white'}}
                useNativeAndroidPickerStyle={false}
              />
            )}
          </AppView>
        </AppView>
      </AppCard>
      {error && touched ? <ValidError {...{error}} /> : null}
    </AppView>
  )
}

const pickerSelectStyles = StyleSheet.create({
  // inputIOS: {
  //   fontSize: 16,
  //   paddingVertical: 12,
  //   paddingHorizontal: 10,
  //   borderWidth: 1,
  //   borderColor: 'gray',
  //   borderRadius: 4,
  //   color: 'black',
  //   paddingRight: 30 // to ensure the text is never behind the icon
  // },
  inputAndroid: {
    color: 'black',
    textAlign: 'right'
  }
})

export default memo(SysPicker)
