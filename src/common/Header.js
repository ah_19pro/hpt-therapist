/* eslint-disable react/prop-types */
import {useNavigation} from '@react-navigation/native'
import React, {useCallback} from 'react'
import Menu from '../assets/images/svg/menu.svg'
import {
  AppIcon,
  AppText,
  AppView,
  colors,
  defStyles,
  iconsType,
  responsiveHeight,
  theme
} from './base/index'
import {moderateScale, responsiveWidth} from './utils/responsiveDimensions'
import Filter from '../assets/images/svg/filter.svg'
import {useSelector} from 'react-redux'
import Dots from '../assets/images/svg/3Dots.svg'

const RightComp = ({
  backBtn,
  disBack,
  navigation,
  menu,
  onBackPress,
  rightIcColor = colors.darkGrey2.color
}) => {
  const handlePress = useCallback(() => {
    if (onBackPress) {
      onBackPress()
    } else navigation.goBack()
  }, [navigation, onBackPress])

  const toggleDrawer = useCallback(() => navigation.toggleDrawer(), [
    navigation
  ])

  if (backBtn) {
    return (
      <AppView touchableView onPress={handlePress} disabled={disBack}>
        <AppIcon
          type={iconsType.ant}
          name={'arrowright'}
          color={rightIcColor}
        />
      </AppView>
    )
  } else if (menu) {
    return (
      <AppView touchableView onPress={toggleDrawer}>
        <Menu height={responsiveHeight(5)} width={responsiveWidth(5)} />
      </AppView>
    )
  }
  return null
}

const CenterComp = ({
  tCtStyle = defStyles.grow,
  hTitle,
  hTStyle = theme.hTStyle
}) => {
  return hTitle ? (
    <AppView style={tCtStyle}>
      <AppText tStyle={hTStyle}>{hTitle}</AppText>
    </AppView>
  ) : null
}

const LeftComp = ({
  bell,
  count,
  dots,
  search,
  filter,
  share,
  leftIcColor = colors.darkGrey2.color,
  onLeftIcPress
}) => {
  return search && filter ? (
    <AppView touchableView onPress={onLeftIcPress}>
      <AppIcon type={iconsType.ant} name={'search1'} size={moderateScale(8)} />
    </AppView>
  ) : (
    <AppView touchableView onPress={onLeftIcPress}>
      {bell ? (
        <AppView centerY style={theme.bellCt}>
          <AppIcon
            type={iconsType.fontAwesome}
            name={'bell-o'}
            size={moderateScale(8)}
          />
          {count ? (
            <AppView absolute center style={theme.notificatCount}>
              <AppText color={colors.white} size={defStyles.size10}>
                {count}
              </AppText>
            </AppView>
          ) : null}
        </AppView>
      ) : dots ? (
        <Dots />
      ) : search ? (
        <AppIcon
          type={iconsType.ant}
          name={'search1'}
          size={moderateScale(8)}
        />
      ) : share ? (
        <AppIcon
          type={iconsType.entypo}
          name={'share'}
          color={leftIcColor}
          size={moderateScale(8)}
        />
      ) : filter ? (
        <Filter fill={colors.black.color} />
      ) : null}
    </AppView>
  )
}

const AppHeader = props => {
  const rtl = useSelector(state => state.lang.rtl)

  const navigation = useNavigation()

  const {
    ctStyle,
    tCtStyle,
    hTitle,
    hTStyle = theme.hTStyle,
    disBack,
    backBtn,
    rightIcColor,
    leftIcColor,
    menu,
    bell,
    count,
    dots,
    search,
    filter,
    share,
    onBackPress,
    onLeftIcPress,
    ...rest
  } = props

  const contStyle = [theme.header, ctStyle]

  return (
    <AppView rowReverse={!rtl} {...{...rest}} style={contStyle}>
      <LeftComp
        {...{bell}}
        {...{count}}
        {...{dots}}
        {...{search}}
        {...{filter}}
        {...{share}}
        {...{leftIcColor}}
        {...{onLeftIcPress}}
      />
      <CenterComp {...{tCtStyle}} {...{hTitle}} {...{hTStyle}} />
      <RightComp
        {...{onBackPress}}
        {...{menu}}
        {...{backBtn}}
        {...{rightIcColor}}
        {...{disBack}}
        {...{navigation}}
      />
    </AppView>
  )
}

export default AppHeader

// const LeftComp = ({
//   bell,
//   count,
//   search,
//   filter,
//   share,
//   leftIcColor = colors.darkGrey2.color,
//   onLeftIcPress
// }) => {
//   if (bell)
//     return (
//       <AppView touchableView style={theme.bellCt} onPress={onLeftIcPress}>
//         <AppIcon
//           type={iconsType.fontAwesome}
//           name={'bell-o'}
//           size={moderateScale(8)}
//         />
//         {count ? (
//           <AppView absolute center style={theme.notificatCount}>
//             <AppText color={colors.white} size={defStyles.size10}>
//               {count}
//             </AppText>
//           </AppView>
//         ) : null}
//       </AppView>
//     )
//   else if (search && filter)
//     return (
//       <AppView row centerX>
//         <AppView touchableView style={defStyles.margEnd3} onPress={() => {}}>
//           <Filter fill={colors.black.color} />
//         </AppView>
//         <AppView touchableView onPress={onLeftIcPress}>
//           <AppIcon
//             type={iconsType.ant}
//             name={'search1'}
//             size={moderateScale(8)}
//           />
//         </AppView>
//       </AppView>
//     )
//   else if (search)
//     return (
//       <AppView touchableView onPress={onLeftIcPress}>
//         <AppIcon
//           type={iconsType.ant}
//           name={'search1'}
//           size={moderateScale(8)}
//         />
//       </AppView>
//     )
//   else if (filter)
//     return (
//       <AppView touchableView onPress={onLeftIcPress}>
//         <Filter fill={colors.black.color} />
//       </AppView>
//     )
//   else if (share)
//     return (
//       <AppView touchableView onPress={onLeftIcPress}>
//         <AppIcon
//           type={iconsType.entypo}
//           name={'share'}
//           color={leftIcColor}
//           size={moderateScale(8)}
//         />
//       </AppView>
//     )
//   return null
// }
