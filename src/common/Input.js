/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {memo, useCallback, useEffect, useState} from 'react'
import {Alert} from 'react-native'
import {TouchableOpacity} from 'react-native'
import {Input} from 'react-native-elements'
import Mandatory from '../components/thpSignUp/Mandatory'
import {AppCard, AppView, defStyles, theme, ValidError} from './base/index'

const TextInput = props => {
  const {
    keyboardType,
    secured,
    ct = defStyles.bot3,
    cardStyle = theme.card,
    ctStyle = theme.inputCt,
    inpCtStyle = theme.input,
    inpStyle = theme.tInpStyle,
    icStyle = defStyles.icInpStyle,
    pholder,
    onChangeText,
    handleCase,
    onBlur,
    title,
    pholderColor = theme.phColor.color,
    labelStyle,
    icon,
    value,
    maxLength,
    multiline,
    onPress,
    error,
    touched,
    schKey,
    setFieldValue,
    setFieldTouched,
    loc
  } = props

  const [isFocused, setIsFocused] = useState(false)

  useEffect(() => {
    if (schKey && loc) {
      setFieldValue(schKey, loc)
    }
    if (handleCase) {
      handleCase(value)
    }
  }, [schKey, loc, setFieldValue, value])

  const handleFocus = useCallback(() => {
    setIsFocused(true)
  }, [isFocused])

  const handleBlur = useCallback(() => {
    setIsFocused(false)
    onBlur
    if (setFieldTouched && schKey) setFieldTouched(schKey, true)
  }, [isFocused, onBlur, setFieldTouched, schKey])

  const _cardStyle = [cardStyle, isFocused ? theme.foucsInp : null]

  return (
    <AppView style={ct}>
      {title ? <Mandatory /> : null}
      <AppCard cardStyle={_cardStyle}>
        <AppView row>
          <Input
            keyboardType={keyboardType}
            secureTextEntry={secured}
            inputStyle={inpStyle}
            containerStyle={ctStyle}
            inputContainerStyle={inpCtStyle}
            placeholder={pholder}
            placeholderTextColor={pholderColor}
            labelStyle={labelStyle}
            textAlign={'right'}
            value={value}
            onChangeText={onChangeText}
            onFocus={handleFocus}
            onBlur={handleBlur}
            maxLength={maxLength}
            multiline={multiline}
          />
          <TouchableOpacity style={icStyle} onPress={onPress}>
            {icon}
          </TouchableOpacity>
        </AppView>
      </AppCard>
      {error && touched ? <ValidError {...{error}} /> : null}
    </AppView>
  )
}

function areEqual(prevProps, nextProps) {
  return (
    prevProps.value === nextProps.value &&
    prevProps.error === nextProps.error &&
    prevProps.touched === nextProps.touched &&
    prevProps.loc === nextProps.loc &&
    prevProps.secured === nextProps.secured
  )
}

export default memo(TextInput, areEqual)
