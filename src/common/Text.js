/* eslint-disable react/prop-types */
import React from 'react'
import {Text} from 'react-native'
import {colors} from './defaults/colors'
// import AppView from './View'
// import {getColors} from './defaults/colors'
import {defStyles} from './defaults/styles'

const AppText = ({
  tStyle,
  bold,
  stretch,
  colStart,
  colEnd,
  size = defStyles.size14,
  color = colors.darkGrey,
  alignStart,
  alignEnd,
  center,
  onTextLayout,
  textDecLine,
  numberOfLines,
  children
}) => {
  const rtl = true

  return (
    <Text
      style={[
        defStyles.normal,
        bold ? defStyles.bold : {},
        size,
        color,
        stretch ? defStyles.stretch : null,
        colStart ? defStyles.colStart : null,
        colEnd ? defStyles.colEnd : null,
        alignStart ? defStyles.alignStart : {},
        alignEnd ? defStyles.alignEnd : {},
        center ? defStyles.alignCenter : {},
        textDecLine ? defStyles.textDecLine : {},
        rtl ? {textAlign: 'right'} : {textAlign: 'left'},
        tStyle
      ]}
      {...{numberOfLines}}
      {...{onTextLayout}}>
      {children}
    </Text>
  )
}

export default AppText
