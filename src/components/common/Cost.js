/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React from 'react'
import {AppText, AppView, colors, defStyles, defSvg} from '../../common/base'
import {styles} from './styles'

const Cost = ({
  cost,
  hrRate,
  ctSyle = styles.ctFees,
  unit = I18n.t('sar/hr')
}) => {
  return (
    <AppView center style={ctSyle}>
      <AppView row center>
        <AppView style={styles.examFees}>
          <AppText
            color={colors.darkGrey3}
            size={defStyles.size20}
            numberOfLines={1}>
            {cost || hrRate}
          </AppText>
        </AppView>
        {defSvg.money}
      </AppView>
      <AppText color={colors.lightGrey3} size={defStyles.size10}>
        {unit}
      </AppText>
    </AppView>
  )
}

export default Cost
