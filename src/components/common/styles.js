import {StyleSheet} from 'react-native'
import {colors, responsiveHeight, responsiveWidth} from '../../common/base'

export const styles = StyleSheet.create({
  ct: {
    height: responsiveHeight(7)
  },
  dialog: {
    height: responsiveHeight(16),
    borderRadius: 10,
    backgroundColor: colors.white.color,
    width: '50%',
    padding: 0,
    margin: 0,
    elevation: 0,
    shadowOpacity: 0,
    shadowColor: colors.white.color
  },
  divider: {
    height: responsiveHeight(0.1),
    backgroundColor: colors.darkGrey.color
  },
  ctFees: {
    width: '25%'
    // backgroundColor: 'green'
  },
  examFees: {
    marginEnd: responsiveWidth(1)
  }
})
