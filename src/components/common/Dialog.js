/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React from 'react'
import {Overlay} from 'react-native-elements'
import {AppText, AppView, defStyles} from '../../common/base/index'
import {styles} from './styles'

const Row = ({title, onPress}) => {
  return (
    <AppView stretch touchableView {...{onPress}}>
      <AppView center style={styles.ct}>
        <AppText size={defStyles.size18}>{title}</AppText>
      </AppView>
    </AppView>
  )
}

const Dialog = ({isVisible, toggleDialog, onEdit, onDelete}) => {
  return (
    <Overlay
      {...{isVisible}}
      onTouchCancel={toggleDialog}
      onBackdropPress={toggleDialog}
      overlayStyle={styles.dialog}>
      <AppView flex center>
        <Row title={I18n.t('edit')} onPress={onEdit} />
        <AppView stretch style={styles.divider} />
        <Row title={I18n.t('delete')} onPress={onDelete} />
      </AppView>
    </Overlay>
  )
}

export default Dialog
