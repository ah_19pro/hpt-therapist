/* eslint-disable react/prop-types */
import {Formik} from 'formik'
import I18n from 'i18n-js'
import moment from 'moment'
import 'moment/locale/ar'
import React, {useCallback, useMemo} from 'react'
import {Alert} from 'react-native'
import {
  AppButton,
  AppInput,
  AppPicker,
  AppText,
  AppView,
  defStyles,
  DTPicker,
  theme,
  ValidError
} from '../../common/base/index'
import {validSchema} from '../../validation/auth/signUp/center/CompSignUp'
import {styles} from './styles'
import Avatar from './UpImage'

function RangeErrors(props) {
  return (
    <AppView row spaceBetween>
      {moment(props.values.wTimeTo, 'HH:mm').isSameOrBefore(
        moment(props.initTTime, 'HH:mm')
      ) ? (
        <AppView />
      ) : (
        <ValidError tStyle={theme.err2} error={I18n.t('mustBeInCrTime')} />
      )}
      {moment(props.values.wTimeFrom, 'HH:mm').isSameOrAfter(
        moment(props.initFTime, 'HH:mm')
      ) ? (
        <AppView />
      ) : (
        <ValidError tStyle={theme.err2} error={I18n.t('mustBeInCrTime')} />
      )}
    </AppView>
  )
}

const AppForm = ({onSubmit, ...rest}) => {
  const {
    _route,
    thpWDTs,
    crData,
    thpDays,
    crDays,
    image,
    upImage,
    imgLoad,
    crCerts,
    crSpecs,
    timesOneText,
    initFTime,
    initTTime,
    loadCerts,
    loadSpecs,
    loadDays,
    loading,
    isVisibFrom,
    isVisibTo,
    navigToSpecs,
    navigToCerts,
    togglePickFrom,
    togglePickTo,
    handleWDays,
    handleWTimes
  } = rest

  return (
    <Formik
      validationSchema={validSchema}
      initialValues={{
        step: crData.steps,
        avatar: '',
        firstName: '',
        lastName: '',
        medCost: '',
        pack7SesPr: '',
        pack12SesPr: '',
        age: '',
        specs: '',
        certs: '',
        courses: '',
        organizs: '',
        wDays: '',
        wTimesText: '',
        fromTimes: '',
        toTimes: '',
        wTimeFrom: initFTime,
        wTimeTo: initTTime
      }}
      onSubmit={onSubmit}>
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        setFieldValue,
        setFieldTouched
      }) => {
        const handlePress = useCallback(() => {
          setFieldTouched('certs', true)
          setFieldTouched('specs', true)
          setFieldTouched('wDays', true)
          setFieldTouched('wTimesText', true)
          setFieldTouched('wTimeFrom', true)
          setFieldTouched('wTimeTo', true)
          handleSubmit()
        }, [values, touched, errors])

        useMemo(() => {
          if (timesOneText) {
            setFieldValue('wTimesText', timesOneText)
          }
        }, [timesOneText])

        useMemo(() => {
          if (thpWDTs) {
            setFieldValue('fromTimes', thpWDTs.from_times)
            setFieldValue('toTimes', thpWDTs.to_times)
          }
        }, [thpWDTs])

        return (
          <>
            <Avatar
              {...{setFieldValue}}
              schKey={'avatar'}
              route={_route}
              title={I18n.t('thpAvatar')}
              onPress={upImage}
              loading={imgLoad}
              image={image || values.avatar}
              error={errors.avatar}
              touched={touched.avatar}
            />
            <AppInput
              {...{setFieldTouched}}
              title={I18n.t('mand')}
              pholder={I18n.t('firstName')}
              onChangeText={handleChange('firstName')}
              onBlur={handleBlur('firstName')}
              value={values.firstName}
              error={errors.firstName}
              touched={touched.firstName}
            />
            <AppInput
              {...{setFieldTouched}}
              title={I18n.t('mand')}
              pholder={I18n.t('lastName')}
              onChangeText={handleChange('lastName')}
              onBlur={handleBlur('lastName')}
              value={values.lastName}
              error={errors.lastName}
              touched={touched.lastName}
            />
            <AppInput
              {...{setFieldTouched}}
              schKey={'medCost'}
              keyboardType={'phone-pad'}
              pholder={`${I18n.t('medCost')} (${I18n.t('sar')})`}
              title={I18n.t('mand')}
              onChangeText={handleChange('medCost')}
              onBlur={handleBlur('medCost')}
              value={values.medCost}
              error={errors.medCost}
              touched={touched.medCost}
            />
            <AppInput
              {...{setFieldTouched}}
              schKey={'pack7SesPr'}
              keyboardType={'phone-pad'}
              pholder={`${I18n.t('pack7Sess')} (${I18n.t('sar')})`}
              title={I18n.t('mand')}
              onChangeText={handleChange('pack7SesPr')}
              onBlur={handleBlur('pack7SesPr')}
              value={values.pack7SesPr}
              error={errors.pack7SesPr}
              touched={touched.pack7SesPr}
            />
            <AppInput
              {...{setFieldTouched}}
              schKey={'pack12SesPr'}
              keyboardType={'phone-pad'}
              pholder={`${I18n.t('pack12Sess')} (${I18n.t('sar')})`}
              title={I18n.t('mand')}
              onChangeText={handleChange('pack12SesPr')}
              onBlur={handleBlur('pack12SesPr')}
              value={values.pack12SesPr}
              error={errors.pack12SesPr}
              touched={touched.pack12SesPr}
            />
            <AppInput
              {...{setFieldTouched}}
              keyboardType={'numeric'}
              pholder={I18n.t('age')}
              onChangeText={handleChange('age')}
              onBlur={handleBlur('age')}
              value={values.age}
              error={errors.age}
              touched={touched.age}
            />
            <AppPicker
              {...{setFieldValue}}
              mandTitle={I18n.t('mand')}
              schKey={'specs'}
              loading={loadSpecs}
              title={I18n.t('healthSpecialites')}
              data={crSpecs}
              onPress={navigToSpecs}
              error={errors.specs}
              touched={touched.specs}
            />
            <AppPicker
              {...{setFieldValue}}
              mandTitle={I18n.t('mand')}
              schKey={'certs'}
              loading={loadCerts}
              title={I18n.t('certificates')}
              data={crCerts}
              onPress={navigToCerts}
              error={errors.certs}
              touched={touched.certs}
            />
            <AppInput
              {...{setFieldTouched}}
              pholder={I18n.t('courses')}
              onChangeText={handleChange('courses')}
              onBlur={handleBlur('courses')}
              value={values.courses}
              error={errors.courses}
              touched={touched.courses}
            />
            <AppInput
              {...{setFieldTouched}}
              pholder={I18n.t('organiz')}
              onChangeText={handleChange('organizs')}
              onBlur={handleBlur('organizs')}
              value={values.organizs}
              error={errors.organizs}
              touched={touched.organizs}
            />
            <AppPicker
              {...{setFieldValue}}
              mandTitle={I18n.t('mand')}
              schKey={'wDays'}
              loading={loadDays}
              title={I18n.t('wDays')}
              data={thpDays || crDays}
              onPress={handleWDays}
              error={errors.wDays}
              touched={touched.wDays}
            />
            {values.step === 2 ? (
              <AppPicker
                {...{setFieldValue}}
                titleStyle={defStyles.width90}
                mandTitle={I18n.t('mand')}
                schKey={'wTimesText'}
                title={values.wTimesText || I18n.t('wTimes')}
                data={''}
                onPress={handleWTimes}
                error={errors.wTimesText}
                touched={touched.wTimesText}
              />
            ) : null}
            {values.step === 1 ? (
              <AppView>
                <AppView style={styles.ctStyle2}>
                  <AppText size={defStyles.size16}>{I18n.t('wTimes')}</AppText>
                </AppView>
                <AppView row centerX spaceBetween>
                  <DTPicker
                    {...{setFieldValue}}
                    mandTitle={I18n.t('mand')}
                    tCtStyle={styles.ct}
                    _style={errors.wTimeTo ? styles.ct11 : {}}
                    pickerCt={{}}
                    schKey={'wTimeTo'}
                    isVisible={isVisibTo}
                    onPress={togglePickTo}
                    onCancel={togglePickTo}
                    title={
                      values.wTimeTo
                        ? moment(values.wTimeTo, 'HH:mm')
                            .locale('ar')
                            .format('hh:mm a')
                        : I18n.t('to')
                    }
                  />
                  <DTPicker
                    {...{setFieldValue}}
                    mandTitle={I18n.t('mand')}
                    tCtStyle={styles.ct}
                    _style={errors.wTimeTo ? styles.ct11 : {}}
                    pickerCt={{}}
                    schKey={'wTimeFrom'}
                    isVisible={isVisibFrom}
                    onPress={togglePickFrom}
                    onCancel={togglePickFrom}
                    title={
                      values.wTimeFrom
                        ? moment(values.wTimeFrom, 'HH:mm')
                            .locale('ar')
                            .format('hh:mm a')
                        : I18n.t('from')
                    }
                  />
                </AppView>
                <AppView row>
                  {errors.wTimeTo && touched.wTimeTo ? (
                    <AppView colEnd style={styles.width45}>
                      <ValidError error={errors.wTimeTo} />
                    </AppView>
                  ) : null}
                  {errors.wTimeTo && touched.wTimeTo ? (
                    <AppView style={styles.width45} />
                  ) : null}
                </AppView>
                <RangeErrors
                  initFTime={initFTime}
                  initTTime={initTTime}
                  values={values}
                />
              </AppView>
            ) : null}
            <AppButton
              ctStyle={styles.btn}
              loading={loading}
              title={I18n.t('accRegister')}
              onPress={handlePress}
            />
          </>
        )
      }}
    </Formik>
  )
}

export default AppForm
