/* eslint-disable react/prop-types */
import {Formik} from 'formik'
import I18n from 'i18n-js'
import moment from 'moment'
import React, {memo, useCallback, useEffect, useMemo} from 'react'
import {Alert} from 'react-native'
import {
  AppButton,
  AppCheckBx,
  AppInput,
  AppPicker,
  AppText,
  AppView,
  colors,
  defIcons,
  defStyles,
  DTPicker,
  responsiveHeight,
  SysPicker,
  theme,
  ValidError
} from '../../common/base/index'
import {validSchema} from '../../validation/auth/signUp/center/SignUp'
import Branch from './Branch'
import CrImages from './CrImages'
import Mandatory from './Mandatory'
import {styles} from './styles'
import UpFile from './UpfFile'
import Loc from '../../assets/images/svg/location.svg'
import 'moment/locale/ar'
import {TouchableOpacity} from 'react-native'

function EmailInput(props) {
  return (
    <AppView>
      <AppInput
        {...{
          setFieldTouched: props.setFieldTouched
        }}
        ct={{}}
        keyboardType={'email-address'}
        title={I18n.t('mand')}
        pholder={I18n.t('email')}
        schKey={'email'}
        onChangeText={props.handleChange('email')}
        handleCase={props.checkEmail}
        onBlur={props.handleBlur('email')}
        value={props.email}
        error={props._email}
        touched={props.__email}
      />
      {props.emAvailable && props.length >= 10 ? (
        <ValidError tStyle={theme.err3} error={I18n.t('unavbEmail')} />
      ) : (
        <AppView style={defStyles.bot2} />
      )}
    </AppView>
  )
}

function PhoneInput(props) {
  return (
    <AppView>
      <AppInput
        {...{
          setFieldTouched: props.setFieldTouched
        }}
        ct={{}}
        pholder={I18n.t('phoneNo')}
        schKey={'phoneNo'}
        title={I18n.t('mand')}
        keyboardType={'phone-pad'}
        onChangeText={props.handleChange('phoneNo')}
        handleCase={props.checkPhoneNo}
        onBlur={props.handleBlur('phoneNo')}
        value={props.phoneNo}
        error={props._phoneNo}
        touched={props.__phoneNo}
      />
      {props.phvailable && props.length >= 10 ? (
        <ValidError tStyle={theme.err3} error={I18n.t('unavbphone')} />
      ) : (
        <AppView style={defStyles.bot2} />
      )}
    </AppView>
  )
}

function CommInput(props) {
  return (
    <AppView>
      <AppInput
        {...{
          setFieldTouched: props.setFieldTouched
        }}
        ct={{}}
        pholder={I18n.t('CommHealthSpecNo')}
        title={I18n.t('mand')}
        schKey={'comHSpecNo'}
        onChangeText={props.handleChange('comHSpecNo')}
        handleCase={props.checkCommission}
        onBlur={props.handleBlur('comHSpecNo')}
        value={props.comHSpecNo}
        error={props._comHSpecNo}
        touched={props.__comHSpecNo}
      />
      {props.commAvailable && props.comHSpecNo.length == 11 ? (
        <ValidError tStyle={theme.err3} error={I18n.t('unavCommNo')} />
      ) : (
        <AppView style={defStyles.bot2} />
      )}
    </AppView>
  )
}

const TimeError = props => {
  const {wTimeFrom: fromErr, wTimeTo: toErr} = props.errors
  const {wTimeFrom: fromTouched, wTimeTo: toTouched} = props.touched

  return (
    <AppView row style={styles.ct9}>
      {toErr && toTouched ? (
        <AppView colEnd style={styles.width45}>
          <ValidError error={toErr} />
        </AppView>
      ) : (
        <AppView style={styles.width45} />
      )}
      {fromErr && fromTouched ? (
        <AppView grow colEnd>
          <ValidError error={fromErr} />
        </AppView>
      ) : (
        <AppView style={styles.width45} />
      )}
    </AppView>
  )
}

const AppForm = ({onSubmit, ...rest}) => {
  const {
    confirms,
    loc,
    navigToMap,
    crs,
    ctrs,
    cits,
    areas,
    crSpecs,
    crInsces,
    checkInsce,
    insceStatus,
    navigToInsces,
    navigToSpecs,
    navigToWDays,
    crDays,
    loadCRs,
    loadCtrys,
    loadCitys,
    loadAreas,
    loadSpecs,
    loadInsces,
    loadComRec,
    loadDays,
    loading,
    comRegist,
    onUpFile,
    handlePreview,
    onCtryPick,
    onCityPick,
    handleImage,
    imgLoad,
    images,
    deleteImg,
    isVisibFrom,
    isVisibTo,
    togglePickFrom,
    togglePickTo,
    handleCheck,
    checked,
    navigToUsrTerms,
    isValid,
    togPassVisib1,
    visible1,
    togPassVisib2,
    visible2,
    emAvailable,
    checkEmail,
    phvailable,
    checkPhoneNo,
    commAvailable,
    checkCommission
  } = rest

  return (
    <Formik
      validationSchema={validSchema}
      initialValues={{
        branchId: 0,
        mainBrId: '',
        crName: '',
        phoneNo: '',
        email: '',
        aboutCR: '',
        comReg: '',
        comHSpecNo: '',
        location: '',
        locName: '',
        ctryId: '',
        cityId: '',
        areaId: '',
        crImages: '',
        specs: [],
        insces: [],
        insce: '',
        commHSpec: '',
        wDays: [],
        wTimeFrom: '',
        wTimeTo: '',
        password: '',
        passConfirm: '',
        termsApprv: ''
      }}
      onSubmit={onSubmit}>
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        setFieldValue,
        setFieldTouched
      }) => {
        const handlePress = useCallback(() => {
          setFieldTouched('ctryId', true)
          setFieldTouched('cityId', true)
          setFieldTouched('areaId', true)
          setFieldTouched('branchId', true)
          setFieldTouched('comReg', true)
          setFieldTouched('crImages', true)
          setFieldTouched('wDays', true)
          setFieldTouched('wTimeFrom', true)
          setFieldTouched('wTimeTo', true)
          setFieldTouched('termsApprv', true)
          console.log('touched obj', touched)
          handleSubmit()
        }, [touched])

        return (
          <>
            <Branch
              {...{setFieldValue}}
              {...{setFieldTouched}}
              mandTitle={I18n.t('mand')}
              schKey={'branchId'}
              data={confirms}
              error={errors.branchId}
              touched={touched.branchId}
            />
            {values.branchId ? (
              <SysPicker
                {...{setFieldValue}}
                {...{setFieldTouched}}
                loading={loadCRs}
                schKey={'mainBrId'}
                title={I18n.t('mand')}
                data={crs}
                hint={I18n.t('chooseMainCrName')}
                value={values.mainBrId}
                error={errors.mainBrId}
                touched={touched.mainBrId}
              />
            ) : null}
            <AppInput
              {...{setFieldTouched}}
              pholder={I18n.t('crName')}
              schKey={'crName'}
              title={I18n.t('mand')}
              onChangeText={handleChange('crName')}
              onBlur={handleBlur('crName')}
              value={values.crName}
              error={errors.crName}
              touched={touched.crName}
            />
            <PhoneInput
              phvailable={phvailable}
              checkPhoneNo={checkPhoneNo}
              handleChange={handleChange}
              handleBlur={handleBlur}
              phoneNo={values.phoneNo}
              length={values.phoneNo.length}
              _phoneNo={errors.phoneNo}
              __phoneNo={touched.phoneNo}
              setFieldTouched={setFieldTouched}
            />
            <EmailInput
              emAvailable={emAvailable}
              checkEmail={checkEmail}
              handleChange={handleChange}
              handleBlur={handleBlur}
              email={values.email}
              length={values.email.length}
              _email={errors.email}
              __email={touched.email}
              setFieldTouched={setFieldTouched}
            />
            <AppInput
              {...{setFieldTouched}}
              cardStyle={styles.card}
              ctStyle={styles.ctStyle3}
              inpCtStyle={styles.ctStyle4}
              inpStyle={styles.tInptStyle}
              schKey={'aboutCR'}
              title={I18n.t('mand')}
              pholder={I18n.t('aboutCr')}
              onChangeText={handleChange('aboutCR')}
              onBlur={handleBlur('aboutCR')}
              value={values.aboutCR}
              error={errors.aboutCR}
              touched={touched.aboutCR}
              multiline={true}
            />
            <UpFile
              {...{setFieldValue}}
              schKey={'comReg'}
              title={I18n.t('mand')}
              file={comRegist}
              loading={loadComRec}
              onPress={onUpFile}
              onPreview={handlePreview}
              error={errors.comReg}
              touched={touched.comReg}
            />
            <CommInput
              commAvailable={commAvailable}
              checkCommission={checkCommission}
              handleChange={handleChange}
              handleBlur={handleBlur}
              comHSpecNo={values.comHSpecNo}
              _comHSpecNo={errors.comHSpecNo}
              __comHSpecNo={touched.comHSpecNo}
              setFieldTouched={setFieldTouched}
            />
            <AppInput
              {...{setFieldValue}}
              {...{setFieldTouched}}
              {...{loc}}
              schKey={'location'}
              title={I18n.t('mand')}
              pholder={I18n.t('location')}
              onChangeText={handleChange('locName')}
              icon={
                <Loc height={responsiveHeight(4)} width={responsiveHeight(4)} />
              }
              onPress={navigToMap}
              onBlur={handleBlur('location')}
              value={values.locName}
              error={errors.location}
              touched={touched.location}
            />
            <SysPicker
              {...{setFieldValue}}
              {...{setFieldTouched}}
              onPick={onCtryPick}
              loading={loadCtrys}
              schKey={'ctryId'}
              title={I18n.t('mand')}
              data={ctrs}
              hint={I18n.t('country')}
              value={values.ctryId}
              error={errors.ctryId}
              touched={touched.ctryId}
            />
            <SysPicker
              {...{setFieldValue}}
              {...{setFieldTouched}}
              onPick={onCityPick}
              loading={loadCitys}
              schKey={'cityId'}
              title={I18n.t('mand')}
              data={cits}
              hint={I18n.t('city')}
              value={values.cityId}
              error={errors.cityId}
              touched={touched.cityId}
            />
            <SysPicker
              {...{setFieldValue}}
              {...{setFieldTouched}}
              loading={loadAreas}
              schKey={'areaId'}
              title={I18n.t('mand')}
              data={areas}
              hint={I18n.t('area')}
              value={values.areaId}
              error={errors.areaId}
              touched={touched.areaId}
            />
            <CrImages
              {...{setFieldValue}}
              {...{images}}
              {...{imgLoad}}
              schKey={'crImages'}
              onPress={handleImage}
              onDelete={deleteImg}
              error={errors.crImages}
              touched={touched.crImages}
            />
            <AppPicker
              {...{setFieldValue}}
              {...{setFieldTouched}}
              schKey={'specs'}
              data={crSpecs}
              loading={loadSpecs}
              title={I18n.t('healthSpecialites')}
              mandTitle={I18n.t('mand')}
              onPress={navigToSpecs}
              error={errors.specs}
              touched={touched.specs}
            />
            <AppPicker
              {...{setFieldValue}}
              schKey={'insces'}
              data={crInsces}
              loading={loadInsces}
              mandTitle={I18n.t('mand')}
              title={I18n.t('insCompany')}
              onPress={navigToInsces}
              error={errors.insces}
              touched={touched.insces}
            />
            {values.insces.length > 0
              ? values.insces.map(value => {
                  return value == 16 ? (
                    <AppView style={styles.ct10}>
                      <AppInput
                        {...{setFieldTouched}}
                        ct={{}}
                        pholder={I18n.t('enterInsComp')}
                        schKey={'insce'}
                        title={I18n.t('mand')}
                        onChangeText={handleChange('insce')}
                        onBlur={handleBlur('insce')}
                        handleCase={checkInsce}
                        value={values.insce}
                        error={errors.insce}
                        touched={touched.insce}
                      />
                      {insceStatus && values.insce ? (
                        <ValidError error={I18n.t('insceRepeat')} />
                      ) : null}
                    </AppView>
                  ) : null
                })
              : null}
            <AppPicker
              {...{setFieldValue}}
              schKey={'wDays'}
              loading={loadDays}
              title={I18n.t('wDays')}
              mandTitle={I18n.t('mand')}
              data={crDays}
              onPress={navigToWDays}
              error={errors.wDays}
              touched={touched.wDays}
            />
            <AppView style={styles.ct5}>
              <AppText size={defStyles.size16}>{I18n.t('wTimes')}</AppText>
            </AppView>
            <AppView row centerX spaceBetween>
              <DTPicker
                {...{setFieldValue}}
                pickerCt={{}}
                tCtStyle={styles.ct}
                schKey={'wTimeTo'}
                mandTitle={I18n.t('mand')}
                isVisible={isVisibTo}
                onPress={togglePickTo}
                onCancel={togglePickTo}
                title={
                  values.wTimeTo
                    ? moment(values.wTimeTo, 'HH:mm')
                        .locale('ar')
                        .format('hh:mm a')
                    : I18n.t('to')
                }
              />
              <DTPicker
                {...{setFieldValue}}
                pickerCt={{}}
                tCtStyle={styles.ct}
                schKey={'wTimeFrom'}
                mandTitle={I18n.t('mand')}
                isVisible={isVisibFrom}
                onPress={togglePickFrom}
                onCancel={togglePickFrom}
                title={
                  values.wTimeFrom
                    ? moment(values.wTimeFrom, 'HH:mm')
                        .locale('ar')
                        .format('hh:mm a')
                    : I18n.t('from')
                }
              />
            </AppView>
            <TimeError errors={errors} touched={touched} />
            <AppInput
              {...{setFieldTouched}}
              pholder={I18n.t('password')}
              schKey={'password'}
              secured={!visible1}
              icon={visible1 ? defIcons.eyeOn : defIcons.eyeOff}
              onPress={togPassVisib1}
              title={I18n.t('mand')}
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              value={values.password}
              error={errors.password}
              touched={touched.password}
            />
            <AppInput
              {...{setFieldTouched}}
              schKey={'passConfirm'}
              title={I18n.t('mand')}
              pholder={I18n.t('reTypePass')}
              secured={!visible2}
              icon={visible2 ? defIcons.eyeOn : defIcons.eyeOff}
              onPress={togPassVisib2}
              onChangeText={handleChange('passConfirm')}
              onBlur={handleBlur('passConfirm')}
              value={values.passConfirm}
              error={errors.passConfirm}
              touched={touched.passConfirm}
            />
            <AppView style={styles.ct4}>
              <Mandatory />
              <AppView row stretch rowEnd centerX>
                <AppView
                  touchableView
                  onPress={navigToUsrTerms}
                  style={styles.ct3}>
                  <AppText color={colors.secondary}>
                    {I18n.t('userTerms')}
                  </AppText>
                </AppView>
                <AppText>{I18n.t('agreeTo')}</AppText>
                <AppCheckBx
                  {...{setFieldValue}}
                  {...{checked}}
                  schKey={'termsApprv'}
                  onPress={handleCheck}
                />
              </AppView>
              {errors.termsApprv && touched.termsApprv ? (
                <ValidError error={errors.termsApprv} />
              ) : null}
            </AppView>
            <AppButton
              loading={loading}
              disabled={isValid}
              title={I18n.t('next')}
              onPress={handlePress}
            />
          </>
        )
      }}
    </Formik>
  )
}

export default memo(AppForm)
