/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React from 'react'
import {
  AppIcon,
  AppText,
  AppView,
  colors,
  defStyles,
  iconsType,
  moderateScale,
  theme
} from '../../common/base/index'

const Mandatory = ({
  tStyle = theme.mandTStyle,
  titCtStle = defStyles.botHalf,
  title
}) => {
  return (
    <AppView row rowEnd style={titCtStle}>
      <AppIcon
        type={iconsType.materialComm}
        name={'multiplication'}
        color={colors.lightRed2.color}
        size={moderateScale(4)}
      />
      <AppText {...{tStyle}}>{title || I18n.t('mand')}</AppText>
    </AppView>
  )
}

export default Mandatory
