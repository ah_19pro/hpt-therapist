import {StyleSheet} from 'react-native'
import {
  responsiveHeight,
  responsiveWidth
} from '../../common/utils/responsiveDimensions'
import {colors} from '../../common/defaults/colors'

export const styles = StyleSheet.create({
  ctStyle: {
    marginTop: responsiveHeight(3)
  },
  ctStyle2: {
    marginStart: responsiveWidth(10)
  },
  tStyle: {
    marginEnd: responsiveWidth(2)
  },
  tStyle2: {
    marginEnd: responsiveWidth(2)
  },
  uncheckGrey: {
    height: 22,
    width: 22,
    borderRadius: 11,
    borderColor: colors.lightGrey3.color,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  outChecked: {
    height: 22,
    width: 22,
    borderRadius: 11,
    borderColor: colors.secondary.color,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  checked: {
    height: 14,
    width: 14,
    borderRadius: 7,
    backgroundColor: colors.secondary.color
  },
  card: {
    height: responsiveHeight(13.5),
    borderRadius: 10,
    backgroundColor: colors.white.color,
    width: '100%',
    padding: 0,
    margin: 0
  },
  ctStyle3: {
    height: responsiveHeight(13),
    paddingHorizontal: 0
  },
  ctStyle4: {
    height: responsiveHeight(13),
    backgroundColor: colors.white.color,
    borderRadius: 10,
    paddingHorizontal: responsiveWidth(4),
    borderColor: colors.white.color,
    justifyContent: 'flex-start'
  },
  tInptStyle: {
    color: colors.black.color,
    fontSize: 14,
    alignSelf: 'flex-start'
  },
  ctStyle5: {
    height: responsiveHeight(5),
    width: responsiveWidth(25),
    backgroundColor: colors.darkGreen.color,
    borderRadius: 8,
    marginEnd: responsiveWidth(1)
  },
  ctStyle6: {
    marginBottom: responsiveHeight(2)
  },
  ctStyle7: {
    marginBottom: responsiveHeight(2)
  },
  ctStyle8: {
    height: responsiveHeight(13),
    width: responsiveHeight(13),
    backgroundColor: colors.white.color,
    borderRadius: 10
  },
  ctStyle9: {
    height: responsiveHeight(12),
    width: responsiveHeight(12),
    borderRadius: 10
  },
  ctStyle10: {
    marginBottom: responsiveHeight(3)
  },

  ct: {
    width: responsiveWidth(43)
  },
  ct2: {
    marginBottom: responsiveHeight(1.5)
  },
  ct3: {
    marginEnd: responsiveWidth(1)
  },
  ct4: {
    marginTop: responsiveHeight(1),
    marginBottom: responsiveHeight(2)
  },
  ct5: {
    marginBottom: responsiveHeight(2)
  },
  ct6: {
    marginBottom: 0
  },
  ct7: {
    marginBottom: responsiveHeight(3.7)
  },
  ct8: {
    marginBottom: responsiveHeight(2)
  },
  ct9: {
    marginBottom: responsiveHeight(3)
  },
  ct10: {
    marginBottom: responsiveHeight(3)
  },
  btn: {
    marginTop: responsiveHeight(3)
  },
  ct11: {
    marginBottom: responsiveHeight(4)
  },
  imgCt: {
    marginEnd: responsiveWidth(2)
  },
  close: {
    top: -4,
    left: -5
  },
  ct12: {
    marginStart: responsiveWidth(2)
  },
  ct13: {
    marginVertical: responsiveHeight(3)
  },
  commerNm: {
    width: responsiveWidth(30),
    height: responsiveHeight(4),
    // backgroundColor: 'pink',
    padding: 0
    // marginStart: responsiveWidth(1)
  },
  width45: {
    width: '45%'
  }
})
