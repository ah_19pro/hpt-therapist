/* eslint-disable react/prop-types */
import React, {memo, useEffect} from 'react'
import {
  AppView,
  AppText,
  AppScrollView,
  colors,
  AppIcon,
  iconsType,
  moderateScale
} from '../../common/base/index'
import {styles} from './styles'
import Mandatory from './Mandatory'
import UpLogo from './UpImage'
import I18n from 'i18n-js'

const Images = props => {
  const {setFieldValue, error, touched, onDelete, images} = props

  return (
    <AppView grow row rowEnd>
      {images.length > 0
        ? images.map((img, index) => {
            return (
              <AppView row colEnd key={index} style={styles.imgCt}>
                <UpLogo
                  {...{
                    setFieldValue
                  }}
                  {...{
                    error
                  }}
                  {...{
                    touched
                  }}
                  image={img}
                />
                <AppView
                  touchableView
                  absolute
                  style={styles.close}
                  onPress={() => onDelete(img)}>
                  <AppIcon
                    type={iconsType.materialComm}
                    name={'close-circle'}
                    color={colors.darkYellow.color}
                    size={moderateScale(12)}
                  />
                </AppView>
              </AppView>
            )
          })
        : null}
    </AppView>
  )
}

const CrImages = props => {
  const {
    setFieldValue,
    schKey,
    images,
    onDelete,
    onPress,
    imgLoad,
    error,
    touched
  } = props

  useEffect(() => {
    if (images) {
      setFieldValue(schKey, images.length > 0 ? images : '')
    }
  }, [images])

  return (
    <AppView>
      <AppView style={styles.ctStyle7}>
        <AppText color={colors.darkGrey3}>{I18n.t('crLogo')}</AppText>
        <Mandatory title={I18n.t('max10Img')} />
      </AppView>
      <AppView stretch>
        <></>
        <AppScrollView grow horizontal showHorInd={false}>
          <AppView stretch row centerX>
            <Images
              setFieldValue={setFieldValue}
              images={images}
              onDelete={onDelete}
              error={error}
              touched={touched}
            />
            {images.length < 10 ? (
              <UpLogo
                {...{onPress}}
                {...{error}}
                {...{touched}}
                loading={imgLoad}
              />
            ) : null}
          </AppView>
        </AppScrollView>
      </AppView>
    </AppView>
  )
}

export default memo(CrImages)
