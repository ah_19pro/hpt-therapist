/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useEffect} from 'react'
import {defIcons} from '../../common/base'
import {colors} from '../../common/defaults/colors'
import {defStyles} from '../../common/defaults/styles'
import Icon from '../../common/Icon'
import Indicator from '../../common/Indicator'
import AppText from '../../common/Text'
import {iconsType} from '../../common/utils/icons'
import ValidError from '../../common/ValidError'
import AppView from '../../common/View'
import Mandatory from './Mandatory'
import {styles} from './styles'

const UpComp = ({onPress, loading}) => {
  return (
    <AppView touchableView center style={styles.ctStyle5} onPress={onPress}>
      {loading ? (
        <Indicator {...{loading}} />
      ) : (
        <AppView row center>
          <AppText
            tStyle={styles.tStyle2}
            color={colors.white}
            size={defStyles.siz10}>
            {I18n.t('upFile')}
          </AppText>
          <Icon
            type={iconsType.feather}
            name={'upload'}
            color={colors.white.color}
            size={20}
          />
        </AppView>
      )}
    </AppView>
  )
}

const UpFile = props => {
  const {
    title,
    file,
    loading,
    schKey,
    setFieldValue,
    onPress,
    onPreview,
    error,
    touched
  } = props

  useEffect(() => {
    if (file) {
      setFieldValue(schKey, file.uri)
    }
  }, [file])

  return (
    <AppView style={styles.ctStyle6}>
      {title ? <Mandatory /> : null}
      <AppView stretch row centerX spaceBetween>
        <AppView row centerX>
          <UpComp onPress={onPress} {...{loading}} />
          {file ? defIcons.checkCircle : null}
          {file ? (
            <AppView
              touchableView
              colStart
              centerY
              style={styles.commerNm}
              onPress={onPreview}>
              <AppText
                color={colors.darkGreen}
                size={defStyles.size12}
                numberOfLines={1}>
                {file.name}
              </AppText>
            </AppView>
          ) : null}
        </AppView>
        <AppText color={colors.darkGrey3}>{I18n.t('commReg')}</AppText>
      </AppView>
      {error && touched ? <ValidError {...{error}} /> : null}
    </AppView>
  )
}

export default UpFile
