/* eslint-disable react/prop-types */
import React, {useCallback, useState, useEffect, memo} from 'react'
import RB from '../../common/RB'
import AppView from '../../common/View'
import AppText from '../../common/Text'
import ValidError from '../../common/ValidError'
import {styles} from './styles'
import Mandatory from './Mandatory'

const RBItem = ({item: {id, name}, pickedId, onSelect}) => {
  const handlePress = useCallback(() => {
    onSelect(id)
  }, [onSelect, id])

  return (
    <AppView
      touchableView
      row
      centerY
      onPress={handlePress}
      style={styles.ctStyle2}>
      <AppText tStyle={styles.tStyle}>{name}</AppText>
      <RB
        {...{id}}
        {...{pickedId}}
        unchecked={styles.uncheckGrey}
        checked={styles.checked}
        outChecked={styles.outChecked}
      />
    </AppView>
  )
}

const Branch = props => {
  const {mandTitle, data, setFieldValue, schKey, error, touched} = props

  const [pickedId, setPickedId] = useState(0)

  useEffect(() => {
    if (schKey && setFieldValue) {
      setFieldValue(schKey, pickedId)
    }
  }, [pickedId])

  const handleSelect = useCallback(
    id => {
      setPickedId(id)
    },

    [pickedId]
  )

  return (
    <AppView style={styles.ctStyle10}>
      <AppView style={styles.ctStyle}>
        {mandTitle ? <Mandatory /> : null}
        <AppView row rowEnd>
          {data.map(item => {
            return (
              <RBItem
                key={item.id}
                {...{item}}
                {...{pickedId}}
                onSelect={handleSelect}
              />
            )
          })}
        </AppView>
      </AppView>
      {error && touched ? <ValidError {...{error}} /> : null}
    </AppView>
  )
}

export default memo(Branch)
