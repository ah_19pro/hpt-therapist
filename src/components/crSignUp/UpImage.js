/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useEffect} from 'react'
import Upload from '../../assets/images/svg/upload.svg'
import {colors} from '../../common/defaults/colors'
import Image from '../../common/Image'
import Indicator from '../../common/Indicator'
import AppText from '../../common/Text'
import ValidError from '../../common/ValidError'
import AppView from '../../common/View'
import {styles} from './styles'
import Mandatory from './Mandatory'
import {responsiveHeight} from '../../common/base'

const UpLogo = props => {
  const {
    onPress,
    loading,
    image,
    error,
    touched,
    schKey,
    setFieldValue,
    title,
    mandTitle,
    route
  } = props

  useEffect(() => {
    if (image && schKey) {
      setFieldValue(schKey, image)
    }
  }, [image])

  return (
    <AppView colEnd style={route ? styles.ct13 : styles.ctStyle6}>
      {title ? (
        <AppView style={styles.ctStyle7}>
          <AppText color={colors.darkGrey3}>{title}</AppText>
        </AppView>
      ) : null}
      <AppView touchableView onPress={onPress}>
        {mandTitle ? <Mandatory /> : null}
        {loading ? (
          <AppView center style={styles.ctStyle8}>
            <Indicator {...{loading}} />
          </AppView>
        ) : image ? (
          <Image ctStyle={styles.ctStyle9} source={{uri: image}} />
        ) : (
          <Upload height={responsiveHeight(13)} width={responsiveHeight(13)} />
        )}
      </AppView>
      {error && touched ? <ValidError {...{error}} /> : null}
    </AppView>
  )
}

export default UpLogo
