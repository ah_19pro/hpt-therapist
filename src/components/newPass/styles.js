import {StyleSheet} from 'react-native'
import {responsiveHeight} from '../../common/utils/responsiveDimensions'
// import {getColors} from './colors'

export const styles = StyleSheet.create({
  bmStyle: {
    marginBottom: responsiveHeight(6)
  }
})
