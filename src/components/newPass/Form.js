/* eslint-disable react/prop-types */
import React from 'react'
import AppInput from '../../common/Input'
import AppButton from '../../common/button'
import I18n from 'i18n-js'
import {Formik} from 'formik'
import {validSchema} from '../../validation/auth/NewPass'
import {styles} from './styles'
import {Alert} from 'react-native'
import {defIcons} from '../../common/base'

const AppForm = ({
  loading,
  onSubmit,
  togPassVisib1,
  visible1,
  togPassVisib2,
  visible2
}) => {
  return (
    <Formik
      validationSchema={validSchema}
      initialValues={{
        password: '',
        passConfirm: ''
      }}
      onSubmit={onSubmit}>
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        setFieldTouched
      }) => (
        <>
          <AppInput
            {...{setFieldTouched}}
            schKey={'password'}
            pholder={I18n.t('newPass')}
            secured={visible1 ? false : true}
            icon={visible1 ? defIcons.eyeOn : defIcons.eyeOff}
            onPress={togPassVisib1}
            onChangeText={handleChange('password')}
            onBlur={handleBlur('password')}
            value={values.password}
            error={errors.password}
            touched={touched.password}
          />
          <AppInput
            {...{setFieldTouched}}
            schKey={'passConfirm'}
            ct={styles.bmStyle}
            pholder={I18n.t('enterNewPass')}
            secured={visible2 ? false : true}
            icon={visible2 ? defIcons.eyeOn : defIcons.eyeOff}
            onPress={togPassVisib2}
            onChangeText={handleChange('passConfirm')}
            onBlur={handleBlur('passConfirm')}
            value={values.passConfirm}
            error={errors.passConfirm}
            touched={touched.passConfirm}
          />
          <AppButton
            onPress={handleSubmit}
            title={I18n.t('confirm')}
            // disabled={!isValid}
            loading={loading}
          />
        </>
      )}
    </Formik>
  )
}

export default AppForm
