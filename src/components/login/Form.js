/* eslint-disable react/prop-types */
import React, {memo} from 'react'
import I18n from 'i18n-js'
import {AppInput, AppButton, iconsType, defIcons} from '../../common/base/index'
import {Formik} from 'formik'
import {validSchema} from '../../validation/auth/Login'

const AppForm = ({loading, onSubmit, togPassVisib, visible}) => {
  return (
    <Formik
      validationSchema={validSchema}
      initialValues={{
        email: '',
        password: ''
      }}
      onSubmit={onSubmit}>
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        setFieldTouched
      }) => (
        <>
          <AppInput
            {...{setFieldTouched}}
            schKey={'email'}
            pholder={I18n.t('email')}
            keyboardType={'email-address'}
            onChangeText={handleChange('email')}
            onBlur={handleBlur('email')}
            value={values.email}
            error={errors.email}
            touched={touched.email}
          />
          <AppInput
            {...{setFieldTouched}}
            schKey={'password'}
            pholder={I18n.t('password')}
            secured={visible ? false : true}
            icon={visible ? defIcons.eyeOn : defIcons.eyeOff}
            onPress={togPassVisib}
            onChangeText={handleChange('password')}
            onBlur={handleBlur('password')}
            value={values.password}
            error={errors.password}
            touched={touched.password}
          />
          <AppButton
            loading={loading}
            title={I18n.t('login')}
            onPress={handleSubmit}
          />
        </>
      )}
    </Formik>
  )
}

export default memo(AppForm)
