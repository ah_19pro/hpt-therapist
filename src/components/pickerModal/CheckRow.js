/* eslint-disable react/prop-types */
import React, {useCallback, useState, useEffect} from 'react'
import {colors} from '../../common/defaults/colors'
import {theme} from '../../common/defaults/theme'
import {defStyles} from '../../common/defaults/styles'
import AppText from '../../common/Text'
import AppView from '../../common/View'
import CheckBox from '../../common/CheckBox'
import {Alert} from 'react-native'
import {responsiveWidth} from '../../common/base'
import {styles} from './styles'

const CheckRow = props => {
  const {
    item: {parent_id, id, name, checkedOther},
    ctStyle = theme.checkRow,
    tStyle = defStyles.width85,
    tColor = colors.darkGrey.color,
    tSize = defStyles.size20,
    onCheck,
    pickedIds
  } = props

  const [checked, setChecked] = useState(false)
  const [disabled, setDisabled] = useState(false)

  useEffect(() => {
    if (pickedIds?.length > 0) {
      pickedIds.forEach(item => {
        const selectedId = item.id //selected before
        const selectedPId = item.parent_id //selected before
        if (
          selectedPId === id ||
          selectedId === id ||
          (selectedPId === parent_id && Number(parent_id))
        ) {
          setChecked(true)
        }
      })
    }
  }, [pickedIds, id])

  useEffect(() => {
    if (typeof checkedOther === 'boolean' && !checkedOther) {
      //disable other buttons when other button checked true
      setDisabled(true)
    } else if (typeof checkedOther === 'boolean' && checkedOther) {
      //enabled disabled buttons when other button checked false
      setDisabled(false)
    }
  }, [checkedOther])

  const handleCheck = useCallback(() => {
    if (disabled) return
    setChecked(!checked)
    onCheck({parent_id, id, name, status: !checked})
  }, [id, parent_id, name, checked, disabled, onCheck])

  return (
    <AppView
      {...{touchableView: !disabled}}
      row
      centerX
      style={ctStyle}
      onPress={handleCheck}>
      <AppView center style={[tStyle, name?.length > 20 ? styles.padEnd : {}]}>
        <AppText color={tColor} size={tSize}>
          {name}
        </AppText>
      </AppView>
      <CheckBox {...{checked}} onPress={handleCheck} {...{disabled}} />
    </AppView>
  )
}

export default CheckRow
