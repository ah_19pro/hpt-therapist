/* eslint-disable react/prop-types */
import React, {useCallback} from 'react'
import {colors} from '../../common/defaults/colors'
import {theme} from '../../common/defaults/theme'
import {defStyles} from '../../common/defaults/styles'
import RB from '../../common/RB'
import AppText from '../../common/Text'
import AppView from '../../common/View'
import {routes} from '../../common/defaults/routes'
import {useNavigation} from '@react-navigation/native'

const Row = props => {
  const navigation = useNavigation()

  const {
    item: {id, name, title, country_id, city_id},
    ctStyle = theme.item,
    tStyle = [defStyles.center, defStyles.grow],
    tColor = colors.darkGrey.color,
    tSize = defStyles.size20,
    pickedId
  } = props

  const handlePick = useCallback(() => {
    navigation.navigate(routes.therapSignUp, {
      id,
      ctryId: country_id,
      cityId: city_id,
      name,
      title
    })
  }, [id, country_id, city_id, name])

  return (
    <AppView touchableView row centerX style={ctStyle} onPress={handlePick}>
      <AppView style={tStyle}>
        <AppText color={tColor} size={tSize}>
          {name}
        </AppText>
      </AppView>
      <RB {...{id}} {...{pickedId}} />
    </AppView>
  )
}

export default Row
