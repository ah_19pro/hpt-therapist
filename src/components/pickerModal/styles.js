import {StyleSheet} from 'react-native'
import {colors} from '../../common/defaults/colors'
import {
  responsiveHeight,
  responsiveWidth
} from '../../common/utils/responsiveDimensions'

export const styles = StyleSheet.create({
  picker: {
    borderColor: colors.lightGreen.color,
    borderWidth: 1
  },
  ct: {
    marginTop: responsiveHeight(2),
    marginBottom: responsiveHeight(3)
  },
  ct2: {
    marginBottom: responsiveHeight(1.5)
  },
  ct3: {
    width: responsiveWidth(43)
  },
  ct4: {
    marginEnd: responsiveWidth(1)
  },
  ct5: {
    marginEnd: responsiveWidth(1)
  },
  ct6: {
    marginBottom: 0
  },
  ct7: {
    marginBottom: responsiveHeight(3)
  },
  ct8: {
    marginBottom: responsiveHeight(1.5)
  },
  width45: {
    width: '45%'
  },
  padEnd: {
    paddingEnd: responsiveWidth(6)
  }
})
