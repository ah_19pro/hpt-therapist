/* eslint-disable react/prop-types */
import {Formik} from 'formik'
import I18n from 'i18n-js'
import React from 'react'
import {AppButton, AppInput, defIcons} from '../../common/base/index'
import {validSchema} from '../../validation/profile/changePassword'

const AppForm = props => {
  const {
    loading,
    onSubmit,
    togVisibOld,
    togVisibNew,
    togVisibConf,
    visibleOld,
    visibleNew,
    visibleConf
  } = props

  return (
    <Formik
      validationSchema={validSchema}
      initialValues={{
        oldPass: '',
        newPass: '',
        passConfirm: ''
      }}
      onSubmit={onSubmit}>
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        setFieldTouched
      }) => (
        <>
          <AppInput
            {...{setFieldTouched}}
            schKey={'oldPass'}
            pholder={I18n.t('oldPass')}
            secured={!visibleOld}
            icon={visibleOld ? defIcons.eyeOn : defIcons.eyeOff}
            onPress={togVisibOld}
            onChangeText={handleChange('oldPass')}
            onBlur={handleBlur('oldPass')}
            value={values.oldPass}
            error={errors.oldPass}
            touched={touched.oldPass}
          />
          <AppInput
            {...{setFieldTouched}}
            schKey={'newPass'}
            pholder={I18n.t('newPass')}
            secured={!visibleNew}
            icon={visibleNew ? defIcons.eyeOn : defIcons.eyeOff}
            onPress={togVisibNew}
            onChangeText={handleChange('newPass')}
            onBlur={handleBlur('newPass')}
            value={values.newPass}
            error={errors.newPass}
            touched={touched.newPass}
          />
          <AppInput
            {...{setFieldTouched}}
            schKey={'passConfirm'}
            pholder={I18n.t('reTypePass')}
            secured={!visibleConf}
            icon={visibleConf ? defIcons.eyeOn : defIcons.eyeOff}
            onPress={togVisibConf}
            onChangeText={handleChange('passConfirm')}
            onBlur={handleBlur('passConfirm')}
            value={values.passConfirm}
            error={errors.passConfirm}
            touched={touched.passConfirm}
          />
          <AppButton
            {...{loading}}
            title={I18n.t('update')}
            onPress={handleSubmit}
          />
        </>
      )}
    </Formik>
  )
}

export default AppForm
