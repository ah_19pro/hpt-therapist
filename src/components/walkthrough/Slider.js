/* eslint-disable react/prop-types */
import React, {memo} from 'react'
import Swiper from 'react-native-swiper'
import {
  AppImage,
  AppView,
  colors,
  images,
  responsiveHeight
} from '../../common/base'
import {styles} from './styles'

const Slider = ({onChange}) => {
  return (
    <AppView style={styles.style2}>
      <Swiper
        height={responsiveHeight(40)}
        autoplayDirection={false}
        style={styles.swiper}
        autoplay
        horizontal
        // loop
        activeDotColor={colors.secondary.color}
        dotColor={colors.lightGrey.color}
        onIndexChanged={onChange}>
        <AppImage alignCr ctStyle={styles.imStyle} source={images.intImg3} />
        <AppImage alignCr ctStyle={styles.imStyle} source={images.intImg2} />
        <AppImage alignCr ctStyle={styles.imStyle} source={images.intImg1} />
      </Swiper>
    </AppView>
  )
}

export default memo(Slider)
