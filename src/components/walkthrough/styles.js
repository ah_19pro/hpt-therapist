import {StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../../common/base'

export const styles = StyleSheet.create({
  style2: {
    marginTop: responsiveHeight(10),
    height: responsiveHeight(40),
    marginBottom: responsiveHeight(13)
  },
  imStyle: {
    height: responsiveHeight(31),
    width: responsiveWidth(45)
  },
  swiper: {
    flexDirection: 'row-reverse'
  }
})
