/* eslint-disable react/prop-types */
import React from 'react'
import AppInput from '../../common/Input'
import AppButton from '../../common/button'
import I18n from 'i18n-js'
import {Formik} from 'formik'
import {validSchema} from '../../validation/auth/ForgetPass'

const AppForm = ({loading, onSubmit}) => {
  return (
    <Formik
      validationSchema={validSchema}
      initialValues={{
        email: ''
      }}
      onSubmit={onSubmit}>
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        setFieldTouched
      }) => (
        <>
          <AppInput
            {...{setFieldTouched}}
            schKey={'email'}
            pholder={I18n.t('email')}
            onChangeText={handleChange('email')}
            onBlur={handleBlur('email')}
            value={values.email}
            keyboardType='email-address'
            error={errors.email}
            touched={touched.email}
          />
          <AppButton
            onPress={handleSubmit}
            title={I18n.t('next')}
            // disabled={!isValid}
            loading={loading}
          />
        </>
      )}
    </Formik>
  )
}

export default AppForm
