import * as yup from 'yup'
import I18n from 'i18n-js'

export const validSchema = () =>
  yup.object().shape({
    password: yup
      .string()
      .matches(/^(?=.*[0-9])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[A-Z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[a-z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[!@#$%^&*()\-_=+{};:,<.>])/, `${I18n.t('have1C1L1Arth')}`)
      .min(8, `${I18n.t('min-items')} 8 ${I18n.t('items')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'no-space-allowed',
        I18n.t('spaces-not-allowed'),
        value => value && !value.includes(' ')
      ),
    passConfirm: yup
      .string()
      .oneOf([yup.ref('password'), null], I18n.t('confirmPassword-invalid'))
      .required(I18n.t('field-required'))
  })
