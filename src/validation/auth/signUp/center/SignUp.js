import * as yup from 'yup'
import I18n from 'i18n-js'
import moment from 'moment'
// import moment from 'moment'

export const validSchema = () =>
  yup.object().shape({
    branchId: yup.number().required(I18n.t('field-required')),
    mainBrId: yup.number().when('branchId', {
      is: 1,
      then: yup.number().required(I18n.t('field-required'))
    }),
    crName: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(50, `${I18n.t('max-chars')} 50 ${I18n.t('char')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'not-empty',
        I18n.t('empty-not-allowed'),
        value => value && value.replace(/\s/g, '').length > 0
      ),
    phoneNo: yup
      .string()
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'is-space-exist',
        I18n.t('spaces-not-allowed'),
        value => value && !value.includes(' ')
      )
      .test(
        'num-not-betweem-9-and-13',
        `${I18n.t('must-be')} ${I18n.t('between')} 9 ${I18n.t(
          'and'
        )} 13 ${I18n.t('number')}`,
        value => value && value.length >= 9 && value.length <= 13
      )
      .test(
        'is-integer',
        `${I18n.t('must-be')} ${I18n.t('number')} ${I18n.t(
          'positive'
        )} ${I18n.t('and')} ${I18n.t('integer')}`,
        value => value && value.match(/[0-9]/g).length === value.length
      ),
    email: yup
      .string()
      .email(I18n.t('email-invalid'))
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'must-between-10-and-50',
        `${I18n.t('must-be')} ${I18n.t('between')} 10 ${I18n.t(
          'and'
        )} 50 ${I18n.t('item')}`,
        value => value && value.length >= 10 && value.length <= 50
      ),
    aboutCR: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(300, `${I18n.t('max-chars')} 300 ${I18n.t('char')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'not-empty',
        I18n.t('empty-not-allowed'),
        value => value && value.replace(/\s/g, '').length > 0
      ),
    comReg: yup.string().required(I18n.t('field-required')),
    comHSpecNo: yup
      .string()
      .min(11, I18n.t('beginW2L9N'))
      .max(11, I18n.t('beginW2L9N'))
      .test('no-empty', I18n.t('field-required'), value => value)
      .test(
        'no-space',
        I18n.t('spaces-not-allowed'),
        value => value && !value.includes(' ')
      )
      .test(
        'start-w-letters',
        I18n.t('beginW2L9N'),
        value =>
          value &&
          value.substr(0, 1).match(/[A-Za-z]/g) &&
          value.substr(1, 2).match(/[A-Za-z]/g)
      )
      .test(
        'numbers-only',
        I18n.t('beginW2L9N'),
        value =>
          (value &&
            Number.isInteger(Number(value.substr(2, 11))) &&
            Number(value.substr(2, 11)) > 0) ||
          (value && value.substr(2, 11) == '000000000')
      ),
    location: yup.object().required(`${I18n.t('field-required')}`),
    ctryId: yup
      .number()
      .typeError(I18n.t('no-choice'))
      .required(I18n.t('field-required')),
    cityId: yup
      .number()
      .typeError(I18n.t('no-choice'))
      .required(I18n.t('field-required')),
    areaId: yup
      .number()
      .typeError(I18n.t('no-choice'))
      .required(I18n.t('field-required')),
    crImages: yup.array().required(I18n.t('field-required')),
    specs: yup
      .array()
      .test(
        'is-length-bigger-than-zero',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    insces: yup
      .array()
      .test(
        'is-length-bigger-than-zero',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    insce: yup
      .string()
      .typeError(`${I18n.t('must-be')} ${I18n.t('chars')}`)
      .when('insces', {
        is: insces => insces && insces.includes(16),
        then: yup
          .string()
          .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
          .max(100, `${I18n.t('max-chars')} 100 ${I18n.t('char')}`)
          .test('not-empty', I18n.t('field-required'), value => value)
          .test(
            'not-empty',
            I18n.t('empty-not-allowed'),
            value => value && value.replace(/\s/g, '').length > 0
          )
      }),
    wDays: yup
      .array()
      .test(
        'is-length-bigger-than-zero',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    wTimeFrom: yup.string().required(I18n.t('field-required')),
    wTimeTo: yup
      .string()
      .test('not-empty', I18n.t('field-required'), value => value)
      .test('is-greater', I18n.t('must-greater'), (toValue, fromValue) =>
        moment(toValue, 'HH:mm').isAfter(
          moment(fromValue.parent.wTimeFrom, 'HH:mm')
        )
      ),
    password: yup
      .string()
      .matches(/^(?=.*[0-9])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[A-Z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[a-z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[!@#$%^&*()\-_=+{};:,<.>])/, `${I18n.t('have1C1L1Arth')}`)
      .min(8, `${I18n.t('min-items')} 8 ${I18n.t('items')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'no-space-allowed',
        I18n.t('spaces-not-allowed'),
        value => value && !value.includes(' ')
      ),
    passConfirm: yup
      .string()
      .oneOf([yup.ref('password'), null], I18n.t('confirmPassword-invalid'))
      .required(I18n.t('field-required')),
    termsApprv: yup
      .bool()
      .test('is-checked', I18n.t('field-required'), value => {
        return value
      })
  })
