import * as yup from 'yup'
import I18n from 'i18n-js'
import moment from 'moment'

export const validSchema = () =>
  yup.object().shape({
    step: yup.number(),
    avatar: yup.string().required(I18n.t('field-required')),
    firstName: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(20, `${I18n.t('max-chars')} 20 ${I18n.t('char')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'not-empty',
        I18n.t('empty-not-allowed'),
        value => value && value.replace(/\s/g, '').length > 0
      ),
    lastName: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(20, `${I18n.t('max-chars')} 20 ${I18n.t('char')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'not-empty',
        I18n.t('empty-not-allowed'),
        value => value && value.replace(/\s/g, '').length > 0
      ),
    medCost: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'less-than-5',
        `${I18n.t('max-numbers')} 5 ${I18n.t('numbers')}`,
        value => value && value.toString().length <= 5
      ),
    pack7SesPr: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .test('not-empty', I18n.t('field-required'), value => value)
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test(
        'less-than-5',
        `${I18n.t('max-numbers')} 5 ${I18n.t('numbers')}`,
        value => value && value.toString().length <= 5
      ),
    pack12SesPr: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .test('not-empty', I18n.t('field-required'), value => value)
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test(
        'less-than-5',
        `${I18n.t('max-numbers')} 5 ${I18n.t('numbers')}`,
        value => value && value.toString().length <= 5
      ),
    age: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test(
        'max-3-digits',
        `${I18n.t('max-numbers')} 3 ${I18n.t('numbers')}`,
        value => (value && value.toString().length <= 3) || !value
      ),
    specs: yup
      .array()
      .test(
        'not-empty',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    certs: yup
      .array()
      .test(
        'not-empty',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    courses: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(300, `${I18n.t('max-chars')} 300 ${I18n.t('char')}`),
    organizs: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(300, `${I18n.t('max-chars')} 300 ${I18n.t('char')}`),
    wDays: yup
      .array()
      .test(
        'not-empty',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    wTimesText: yup
      .string()
      .nullable()
      .when('step', {
        is: 2,
        then: yup
          .string()
          .test('not-empty', I18n.t('field-required'), value => value)
          .nullable()
      }),
    wTimeFrom: yup
      .string()
      .nullable()
      .when('step', {
        is: 1,
        then: yup.string().required(I18n.t('field-required')).nullable()
      }),
    wTimeTo: yup
      .string()
      .when('step', {
        is: 1,
        then: yup
          .string()
          .test('not-empty', I18n.t('field-required'), value => value)
          .test('is-greater', I18n.t('must-greater'), (toValue, fromValue) => {
            return moment(toValue, 'HH:mm').isAfter(
              moment(fromValue.parent.wTimeFrom, 'HH:mm')
            )
          })
          .nullable()
      })
      .nullable()
  })
