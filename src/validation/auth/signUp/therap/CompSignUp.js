import * as yup from 'yup'
import I18n from 'i18n-js'
import {Alert} from 'react-native'

export const validSchema = () =>
  yup.object().shape({
    medCost: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'less-than-5',
        `${I18n.t('max-numbers')} 5 ${I18n.t('numbers')}`,
        value => value && value.toString().length <= 5
      ),
    pack7SesPr: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'less-than-5',
        `${I18n.t('max-numbers')} 5 ${I18n.t('numbers')}`,
        value => value && value.toString().length <= 5
      ),
    pack12SesPr: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'less-than-5',
        `${I18n.t('max-numbers')} 5 ${I18n.t('numbers')}`,
        value => value && value.toString().length <= 5
      )
  })
