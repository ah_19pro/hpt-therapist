import * as yup from 'yup'
import I18n from 'i18n-js'

export const validSchema = () =>
  yup.object().shape({
    firstName: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(20, `${I18n.t('max-chars')} 20 ${I18n.t('char')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'not-empty',
        I18n.t('empty-not-allowed'),
        value => value && value.replace(/\s/g, '').length > 0
      ),
    lastName: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(20, `${I18n.t('max-chars')} 20 ${I18n.t('char')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'not-empty',
        I18n.t('empty-not-allowed'),
        value => value && value.replace(/\s/g, '').length > 0
      ),
    titleId: yup
      .number(`${I18n.t('no-choice')}`)
      .required(I18n.t('field-required')),
    email: yup
      .string()
      .email(I18n.t('email-invalid'))
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'must-between-10-and-50',
        `${I18n.t('must-be')} ${I18n.t('between')} 10 ${I18n.t(
          'and'
        )} 50 ${I18n.t('item')}`,
        value => value && value.length >= 10 && value.length <= 50
      ),
    phoneNo: yup
      .string()
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'is-space-exist',
        I18n.t('spaces-not-allowed'),
        value => value && !value.includes(' ')
      )
      .test(
        'num-not-betweem-9-and-13',
        `${I18n.t('must-be')} ${I18n.t('between')} 9 ${I18n.t(
          'and'
        )} 13 ${I18n.t('number')}`,
        value => value && value.length >= 9 && value.length <= 13
      )
      .test(
        'is-integer',
        `${I18n.t('must-be')} ${I18n.t('number')} ${I18n.t(
          'positive'
        )} ${I18n.t('and')} ${I18n.t('integer')}`,
        value => value && value.match(/[0-9]/g).length === value.length
      ),
    age: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(`${I18n.t('must-be')} ${I18n.t('integer')}`)
      .positive(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .test(
        'max-3-digits',
        `${I18n.t('max-numbers')} 3 ${I18n.t('numbers')}`,
        value => (value && value.toString().length <= 3) || !value
      ),
    commHealthSpecNo: yup
      .string()
      .min(11, I18n.t('beginW2L9N'))
      .max(11, I18n.t('beginW2L9N'))
      .test('no-empty', I18n.t('field-required'), value => value)
      .test(
        'no-space',
        I18n.t('spaces-not-allowed'),
        value => value && !value.includes(' ')
      )
      .test(
        'start-w-letters',
        I18n.t('beginW2L9N'),
        value =>
          value &&
          value.substr(0, 1).match(/[A-Za-z]/g) &&
          value.substr(1, 2).match(/[A-Za-z]/g)
      )
      .test(
        'numbers-only',
        I18n.t('beginW2L9N'),
        value =>
          (value &&
            Number.isInteger(Number(value.substr(2, 11))) &&
            Number(value.substr(2, 11)) > 0) ||
          (value && value.substr(2, 11) == '000000000')
      ),
    location: yup.object().required(`${I18n.t('field-required')}`),
    ctryId: yup
      .number()
      .typeError(I18n.t('no-choice'))
      .required(I18n.t('field-required')),
    cityId: yup
      .number()
      .typeError(I18n.t('no-choice'))
      .required(I18n.t('field-required')),
    areaId: yup
      .number()
      .typeError(I18n.t('no-choice'))
      .required(I18n.t('field-required'))
  })
