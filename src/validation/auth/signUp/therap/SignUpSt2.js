import * as yup from 'yup'
import I18n from 'i18n-js'
import moment from 'moment'

export const validSchema = () =>
  yup.object().shape({
    teleThp: yup.number().required(I18n.t('field-required')),
    hrRate: yup
      .number()
      .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
      .integer(
        `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
      )
      .positive(`${I18n.t('must-be')} ${I18n.t('positive')}`)
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
      )
      .when('teleThp', {
        is: 1,
        then: yup
          .number()
          .typeError(`${I18n.t('must-be')} ${I18n.t('numbers')}`)
          .integer(
            `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
          )
          .positive(
            `${I18n.t('must-be')} ${I18n.t('bigger-than')} ${I18n.t('zero')}`
          )
          .transform((value, originalValue) =>
            /\s/.test(originalValue) ? I18n.t('spaces-not-allowed') : value
          )
          .test('not-empty', I18n.t('field-required'), value => value)
          .test(
            'less-than-5',
            `${I18n.t('max-numbers')} 5 ${I18n.t('numbers')}`,
            value => value && value.toString().length <= 5
          )
      }),
    certs: yup
      .array()
      .test(
        'not-empty',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    specials: yup
      .array()
      .test(
        'not-empty',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    courses: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(300, `${I18n.t('max-chars')} 300 ${I18n.t('char')}`),
    organizs: yup
      .string()
      .min(3, `${I18n.t('min-chars')} 3 ${I18n.t('char')}`)
      .max(300, `${I18n.t('max-chars')} 300 ${I18n.t('char')}`),
    wDays: yup
      .array()
      .test(
        'not-empty',
        I18n.t('field-required'),
        value => value && value.length > 0
      ),
    wTimeFrom: yup.string().required(I18n.t('field-required')),
    wTimeTo: yup
      .string()
      .test('not-empty', I18n.t('field-required'), value => value)
      .test('is-greater', I18n.t('must-greater'), (toValue, fromValue) =>
        moment(toValue, 'HH:mm').isAfter(
          moment(fromValue.parent.wTimeFrom, 'HH:mm')
        )
      ),
    password: yup
      .string()
      .matches(/^(?=.*[0-9])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[A-Z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[a-z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[!@#$%^&*()\-_=+{};:,<.>])/, `${I18n.t('have1C1L1Arth')}`)
      .min(8, `${I18n.t('min-items')} 8 ${I18n.t('items')}`)
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'no-space-allowed',
        I18n.t('spaces-not-allowed'),
        value => value && !value.includes(' ')
      ),
    passConfirm: yup
      .string()
      .oneOf([yup.ref('password'), null], I18n.t('confirmPassword-invalid'))
      .required(I18n.t('field-required')),
    termsApprv: yup
      .bool()
      .test('is-checked', I18n.t('field-required'), value => value)
  })
