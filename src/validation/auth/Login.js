import * as yup from 'yup'
import I18n from 'i18n-js'

export const validSchema = () =>
  yup.object().shape({
    email: yup
      .string()
      .email(I18n.t('email-invalid'))
      .test('not-empty', I18n.t('field-required'), value => value)
      .test(
        'must-between-10-and-50',
        `${I18n.t('must-be')} ${I18n.t('between')} 10 ${I18n.t(
          'and'
        )} 50 ${I18n.t('item')}`,
        value => value && value.length >= 10 && value.length <= 50
      ),
    password: yup
      .string()
      .matches(/^(?=.*[0-9])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[A-Z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[a-z])/, `${I18n.t('have1C1L1Arth')}`)
      .matches(/^(?=.*[!@#$%^&*()\-_=+{};:,<.>])/, `${I18n.t('have1C1L1Arth')}`)
      .min(8, `${I18n.t('min-items')} 8 ${I18n.t('items')}`)
      .required(I18n.t('field-required'))
  })
