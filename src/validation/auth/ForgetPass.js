import * as yup from 'yup'
import I18n from 'i18n-js'

export const validSchema = () =>
  yup.object().shape({
    email: yup
      .string()
      .required(I18n.t('field-required'))
      .email(I18n.t('email-invalid'))
  })
