import {
  SET_FCM_TOKEN,
  SET_NOTIFICATION,
  SET_NOTIFICATION_COUNT
} from '../types/types'

const initState = {
  fcmToken: null,
  data: null,
  unReadCount: null
}

const notification = (state = initState, action) => {
  switch (action.type) {
    case SET_FCM_TOKEN:
      return {...state, fcmToken: action.fcmToken}
    case SET_NOTIFICATION:
      return {...state, data: action.data}
    case SET_NOTIFICATION_COUNT:
      return {...state, unReadCount: action.unReadCount}
    default:
      return state
  }
}

export default notification
