import {SET_USER_DATA, SET_CR_THP_DATA} from '../types/types'

const initState = {
  data: null,
  crThpData: null
}

export default (state = initState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return {...state, data: action.data}
    case SET_CR_THP_DATA:
      return {...state, crThpData: action.data}
    default:
      return state
  }
}
