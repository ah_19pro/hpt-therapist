import {SET_LANG} from '../types/types'

const initState = {
  rtl: false
}

export default (state = initState, action) => {
  switch (action.type) {
    case SET_LANG:
      return {...state, rtl: action.rtl}
    default:
      return state
  }
}
