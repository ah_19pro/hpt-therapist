import {REFRESH_LIST} from '../types/types'

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case REFRESH_LIST: {
      // const x = {[action.list]: !state[action.list]}
      // console.log('🚀 ~ file: list.js ~ line 15 ~ REFRESH_LIST', x)
      return {...state, [action.list]: !state[action.list]}
    }
    default:
      return state
  }
}
