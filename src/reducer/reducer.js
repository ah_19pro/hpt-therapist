import {combineReducers} from 'redux'
import lang from './lang'
import user from './user'
import list from './list'
import notification from './notification'

export default combineReducers({lang, user, list, notification})
