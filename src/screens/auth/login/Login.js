/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useCallback, useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {setUserData} from '../../../actions/user'
import {CONNECTION_ERROR} from '../../../api/errorTypes'
import {userLogin} from '../../../api/index'
import {colors} from '../../../common/defaults/colors'
import {routes} from '../../../common/defaults/routes'
import {strings} from '../../../common/defaults/strings'
import {defStyles} from '../../../common/defaults/styles'
import {theme} from '../../../common/defaults/theme'
import AppHeader from '../../../common/Header'
import SafeArea from '../../../common/SafeArea'
import AppText from '../../../common/Text'
import {showError, showInfo} from '../../../common/utils/localNotifications'
import AppView from '../../../common/View'
import AppForm from '../../../components/login/Form'
import {saveUserData} from '../../../helper/localStorage'
import {styles} from '../login/styles'
// import {runFcm} from '../../../utils/notificatService'
import {AppStatusBar} from '../../../common/base'

const Login = ({navigation, route}) => {
  let _userType
  const _params = route.params
  if (_params) {
    _userType = _params.userType
  }

  const rtl = useSelector(state => state.lang.rtl)

  // const fcmToken = useSelector(state => state.notification.fcmToken)
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false)
  const [visible, setVisible] = useState(false)

  // useEffect(() => {
  //   if (!fcmToken) {
  //     runFcm()
  //   }
  // }, [fcmToken])

  const onSubmit = useCallback(
    async (values, setSubmitting) => {
      try {
        setLoading(true)
        const res = await userLogin({
          ...values,
          user_type: _userType
          // mobile_token: fcmToken
        })

        dispatch(setUserData(res.data))

        const {id, active, steps, hold, user_type, email} = res.data

        if (!hold) {
          if (active == 0) {
            //email not confirmed
            navigation.navigate(routes.otp, {
              userType: user_type,
              id,
              email,
              active,
              steps
            })
          } else if (active == 1 && steps == 1 && user_type == strings.therap) {
            //email confirmed && navig to thp complete register
            navigation.navigate(routes.thpCompSignUp, {
              id
            })
          } else if (active == 1 && steps == 1 && user_type == strings.center) {
            //email confirmed && navig to center complete register
            navigation.navigate(routes.crCompSignUp, {
              id
            })
          } else if (active !== 2) {
            //email confirmed && register completed but not activated from admin
            showInfo(I18n.t('acctNotActive'))
          } else {
            // navigation.navigate(routes.drawer, {
            //   screen: routes.main,
            //   params: {screen: routes.home}
            // })
            saveUserData(res.data)
          }
        } else {
          showInfo(I18n.t('accIsHold'))
        }
      } catch (error) {
        if (error.type === CONNECTION_ERROR) {
          showError(I18n.t('ui-networkConnectionError'))
        } else {
          showError(error.message)
        }
      } finally {
        setLoading(false)
        setSubmitting
      }
    },
    [_userType]
  )

  const navigToForgetPass = useCallback(() => {
    navigation.navigate(routes.forgetPass, {userType: _userType})
  }, [_userType])

  const navigToSignUp = useCallback(() => {
    if (_userType === strings.therap) {
      navigation.navigate(routes.thpSignUpSt1)
    } else if (_userType === strings.teleThp) {
      navigation.navigate(routes.thpSignUpSt1, {userType: strings.teleThp})
    } else {
      navigation.navigate(routes.crSignUp)
    }
  }, [_userType])

  const togPassVisib = useCallback(() => {
    setVisible(visible => !visible)
  }, [visible])

  return (
    <SafeArea flex>
      <AppStatusBar />
      <AppHeader row rowEnd centerX backBtn />
      <AppView grow style={theme.defPadding}>
        <AppView style={styles.ctStyle1} row rowEnd={rtl}>
          <AppText size={defStyles.size20}>{I18n.t('welcomeAgain')}</AppText>
        </AppView>
        <AppView stretch style={styles.ctStyle2}>
          <AppText size={defStyles.size14}>{I18n.t('loginWEmail')}</AppText>
        </AppView>
        <AppForm
          {...{loading}}
          {...{togPassVisib}}
          {...{visible}}
          {...{onSubmit}}
        />
        <AppView
          touchableView
          stretch
          center
          style={styles.ctStyle4}
          onPress={navigToForgetPass}>
          <AppText color={colors.darkGrey}>{I18n.t('forgotPass')}</AppText>
        </AppView>
        <AppView row alignCenter style={styles.ctStyle3}>
          <AppView touchableView onPress={navigToSignUp}>
            <AppText color={colors.darkGrey} tStyle={styles.tStyle}>
              {I18n.t('createNewAcc')}
            </AppText>
          </AppView>
          <AppText color={colors.lightGrey}>{I18n.t('firstTime')}</AppText>
        </AppView>
      </AppView>
    </SafeArea>
  )
}

export default Login
