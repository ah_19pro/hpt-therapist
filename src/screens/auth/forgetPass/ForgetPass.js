/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useCallback, useState} from 'react'
import {checkUser} from '../../../api/auth/userCheck'
import {CONNECTION_ERROR} from '../../../api/errorTypes'
import {strings} from '../../../common/base'
import {routes} from '../../../common/defaults/routes'
import {defStyles} from '../../../common/defaults/styles'
import {theme} from '../../../common/defaults/theme'
import AppHeader from '../../../common/Header'
import SafeArea from '../../../common/SafeArea'
import AppText from '../../../common/Text'
import {showError, showInfo} from '../../../common/utils/localNotifications'
import AppView from '../../../common/View'
import AppForm from '../../../components/forgetPass/Form'
import {styles} from './styles'

const ForgetPass = ({navigation, route}) => {
  let userType
  const _params = route.params

  if (_params) {
    userType = _params.userType
  }

  const [loading, setLoading] = useState(false)

  const onSubmit = useCallback(async (values, setSubmitting) => {
    try {
      setLoading(true)
      const {email} = values
      const res = await checkUser('email', email, userType)
      const {data, user_id, user_type, active, steps} = res
      if (userType === user_type) {
        if (data && active == 0) {
          //code not confirmed
          // showInfo(I18n.t('plsConfAcc'))
          navigation.navigate(routes.otp, {
            email,
            userType: user_type,
            id: user_id
          })
        } else if (
          data &&
          active == 1 &&
          steps == 1 &&
          user_type == strings.therap
        ) {
          navigation.navigate(routes.thpCompSignUp, {
            id: user_id
          })
        } else if (
          data &&
          active == 1 &&
          steps == 1 &&
          user_type == strings.center
        ) {
          navigation.navigate(routes.crCompSignUp, {
            id: user_id
          })
        } else if (data && steps === 2 && active !== 2) {
          showInfo(I18n.t('acctNotActive'))
        } else if (data) {
          // showInfo(I18n.t('plsConfAcc'))
          navigation.navigate(routes.otp, {
            email: values.email,
            active,
            userType: user_type
          })
        } else {
          showError(I18n.t('userNotExist'))
        }
      } else {
        showError(I18n.t('userNotExist'))
      }
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoading(false)
      setSubmitting
    }
  }, [])

  return (
    <SafeArea flex>
      <AppHeader row rowEnd centerX backBtn />
      <AppView flex style={theme.defPadding}>
        <AppView stretch style={styles.ctStyle1}>
          <AppText size={defStyles.size20}>{I18n.t('forgotPass')}</AppText>
        </AppView>
        <AppView stretch style={styles.ctStyle2}>
          <AppText size={defStyles.size14}>
            {I18n.t('enterEmailFConfirm')}
          </AppText>
        </AppView>
        <AppForm loading={loading} onSubmit={onSubmit} />
      </AppView>
    </SafeArea>
  )
}

export default ForgetPass
