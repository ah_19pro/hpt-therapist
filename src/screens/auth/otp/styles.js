import {
  responsiveHeight,
  responsiveWidth
  // responsiveWidth
} from '../../../common/utils/responsiveDimensions'
import {StyleSheet} from 'react-native'
import {colors} from '../../../common/defaults/colors'

export const styles = StyleSheet.create({
  inpCtStyle: {
    backgroundColor: colors.white.color,
    borderWidth: 1,
    borderRadius: 10
  },
  ctStyle: {
    marginTop: responsiveHeight(6),
    flexGrow: 0.3
    // height: responsiveHeight(10)
  },
  ctStyle2: {
    marginTop: responsiveHeight(5)
  },
  tStyle: {
    marginEnd: responsiveWidth(1.5)
  },
  digStyle: {
    height: responsiveHeight(6),
    width: responsiveWidth(6)
  },
  tStyle2: {
    color: colors.lightGrey.color
  }
})
