/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useCallback, useEffect, useState} from 'react'
import CodeInput from 'react-native-confirmation-code-input'
import CountDown from 'react-native-countdown-component'
import {CONNECTION_ERROR} from '../../../api/errorTypes'
import {checkCode, checkOTP, sendCode} from '../../../api/index'
import {
  AppButton,
  AppHeader,
  AppScrollView,
  AppText,
  AppView,
  colors,
  defStyles,
  routes,
  SafeArea,
  showError,
  showInfo,
  showSuccess,
  strings,
  theme
} from '../../../common/base/index'
import {styles} from './styles'

const Counter = ({count, isActive, onFinish}) => {
  return !isActive ? (
    <CountDown
      until={count}
      onFinish={onFinish}
      digitStyle={styles.digStyle}
      digitTxtStyle={styles.tStyle2}
      timeToShow={['H', 'M', 'S']}
      timeLabels={{m: null, s: null}}
      size={13}
      showSeparator
    />
  ) : null
}

const OTPInput = ({handleCode}) => {
  return (
    <AppView style={styles.ctStyle}>
      <CodeInput
        keyboardType='numeric'
        codeInputStyle={styles.inpCtStyle}
        codeLength={4}
        className={'border-box'}
        activeColor={colors.secondary.color}
        inactiveColor={colors.white.color}
        space={15}
        size={61}
        inputPosition='center'
        onFulfill={handleCode}
      />
    </AppView>
  )
}

const OTP = ({navigation, route}) => {
  const {email, id, userType, active, steps} = route.params

  const [loading, setLoading] = useState(false)
  const [code, setCode] = useState('')
  const [isActive, setIsActive] = useState(true)
  const [count, setCount] = useState(0)

  useEffect(() => {
    if (email) sendUserCode()
  }, [email])

  const sendUserCode = useCallback(async () => {
    try {
      if (isActive) {
        setIsActive(false)
        const res = await sendCode({email})
        if (res.msg == 'your account deactivated for 1 hour') {
          await getRemTime()
          showInfo(res.msg)
        } else {
          setCount(60)
          showSuccess(res.msg)
        }
      }
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    }
  }, [isActive, email, count, getRemTime])

  const getRemTime = async () => {
    try {
      const res = await checkOTP({email})
      console.log(
        '🚀 ~ file: OTP.js ~ line 98 ~ getRemTime ~ res.data',
        res.data
      )
      setCount(res.data / 1000)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    }
  }

  const handleCode = useCallback(value => setCode(value), [code])

  const validCode = useCallback(async () => {
    try {
      setLoading(true)
      const res = await checkCode({email, code})
      showSuccess(res.msg)
      if (userType === strings.therap && !active) {
        navigation.navigate(routes.thpCompSignUp, {id})
      } else if (userType === strings.teleThp && steps == 1 && !active) {
        navigation.navigate(routes.login)
      } else if (userType === strings.center && !active) {
        navigation.navigate(routes.crCompSignUp, {id})
      } else {
        navigation.navigate(routes.newPass, {email})
      }
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoading(false)
    }
  }, [navigation, loading, code, email])

  const actResend = useCallback(() => {
    setIsActive(true)
  }, [isActive])

  const handleNavig = useCallback(() => {
    // Alert.alert('userType', JSON.stringify(userType))
    if (userType) navigation.navigate(routes.login)
    else navigation.goBack()
  }, [userType, navigation])

  return (
    <SafeArea flex>
      <AppHeader row rowEnd centerX backBtn onBackPress={handleNavig} />
      <AppScrollView grow>
        <AppView flex style={theme.defPadding}>
          <AppView stretch>
            <AppText size={defStyles.size20}>{I18n.t('checkEmail')}</AppText>
          </AppView>
          <AppView stretch>
            <AppText size={defStyles.size14}>
              {I18n.t('msgToCheckYEmail')}
            </AppText>
          </AppView>
          <AppText size={defStyles.size16}>{email}</AppText>
          <OTPInput handleCode={handleCode} />
          <AppButton
            title={I18n.t('next')}
            loading={loading}
            disabled={!code}
            onPress={validCode}
          />
          <AppView stretch row rowEnd centerX style={styles.ctStyle2}>
            <Counter {...{count}} {...{isActive}} onFinish={actResend} />
            <AppView touchableView style={styles.tStyle} onPress={sendUserCode}>
              <AppText color={isActive ? colors.secondary : colors.lightGrey}>
                {isActive ? I18n.t('resend') : I18n.t('resendAfter')}
              </AppText>
            </AppView>
            <AppText>{I18n.t('didntRecCode')}</AppText>
          </AppView>
        </AppView>
      </AppScrollView>
    </SafeArea>
  )
}

export default OTP
