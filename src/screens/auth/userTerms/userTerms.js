import React from 'react'
import AppView from '../../../common/View'
import AppText from '../../../common/Text'
import I18n from 'i18n-js'
import SafeArea from '../../../common/SafeArea'
import AppHeader from '../../../common/Header'
import {defStyles} from '../../../common/defaults/styles'

const UserTerms = () => {
  return (
    <SafeArea flex>
      <AppHeader row rowEnd centerX backBtn />
      <AppView stretch center>
        <AppText size={defStyles.size20}>{I18n.t('userTerms')}</AppText>
      </AppView>
    </SafeArea>
  )
}

export default UserTerms
