import {
  responsiveHeight,
  responsiveWidth
} from '../../../common/utils/responsiveDimensions'
import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
  igStyle: {
    height: responsiveHeight(20),
    width: responsiveWidth(80),
    marginTop: responsiveHeight(8)
  },
  igStyle2: {
    height: responsiveHeight(20),
    width: responsiveWidth(90)
  },
  ctStyle: {
    paddingHorizontal: responsiveWidth(12)
  },
  tStyle: {
    marginTop: responsiveHeight(1)
  },
  ct2: {
    position: 'absolute',
    right: responsiveWidth(15),
    top: '37%'
    // backgroundColor: 'pink'
  },
  bStyle: {
    marginTop: responsiveHeight(7)
  },
  ct: {
    marginTop: -responsiveHeight(6.5)
  }
})
