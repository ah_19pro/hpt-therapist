/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useCallback, useEffect} from 'react'
import {BackHandler} from 'react-native'
import TeleThp from '../../..//assets/images/svg/telethp.svg'
import Therapist from '../../..//assets/images/svg/therapist.svg'
import {AppScrollView, AppStatusBar} from '../../../common/base'
import {colors} from '../../../common/defaults/colors'
import {images} from '../../../common/defaults/images'
import {routes} from '../../../common/defaults/routes'
import {strings} from '../../../common/defaults/strings'
import {defStyles} from '../../../common/defaults/styles'
import AppImage from '../../../common/Image'
import SafeArea from '../../../common/SafeArea'
import AppText from '../../../common/Text'
import {responsiveWidth} from '../../../common/utils/responsiveDimensions'
import AppView from '../../../common/View'
import {styles} from './styles'

const SelectAcctType = ({navigation}) => {
  useEffect(() => {
    addBHListener()
    return () => rmBHistener()
  }, [])

  const addBHListener = () => {
    BackHandler.addEventListener('hardwareBackPress', () => true)
  }

  const rmBHistener = () => {
    BackHandler.removeEventListener('hardwareBackPress', () => true)
  }

  const navigToLoginCR = useCallback(
    () => navigation.navigate(routes.login, {userType: strings.center}),
    []
  )

  return (
    <SafeArea flex>
      <AppStatusBar />
      <AppScrollView>
        <AppView alignCenter centerX>
          <AppImage
            imgStyle={styles.igStyle}
            source={images.logo}
            resizeMode={'stretch'}
          />
          <AppText size={defStyles.size32} color={colors.darkGrey}>
            {I18n.t('welcome')}
          </AppText>
          <AppText
            size={defStyles.size18}
            color={colors.lightGrey2}
            tStyle={styles.tStyle}>
            {I18n.t('selectAcctType')}
          </AppText>
        </AppView>
        <AppView
          touchableView
          centerX
          style={styles.ct}
          onPress={navigToLoginCR}>
          <Therapist width={responsiveWidth(95)} />
          <AppView style={styles.ct2}>
            <AppText size={defStyles.size17} color={colors.white}>
              {`${I18n.t('physicalThp')} ${I18n.t('center')}`}
            </AppText>
          </AppView>
        </AppView>
      </AppScrollView>
    </SafeArea>
  )
}

export default SelectAcctType
