/* eslint-disable react/prop-types */
import {useFocusEffect} from '@react-navigation/native'
import I18n from 'i18n-js'
import React, {useCallback, useEffect, useState} from 'react'
import {Alert} from 'react-native'
import {launchImageLibrary} from 'react-native-image-picker'
import ImageResizer from 'react-native-image-resizer'
import {useDispatch} from 'react-redux'
import {setUserData} from '../../../actions/user'
import {CONNECTION_ERROR} from '../../../api/errorTypes'
import {
  centerSignUp,
  checkInsurance,
  checkUser,
  getAllDays,
  getAreas,
  getCenters,
  getCities,
  getCountries,
  getInsurances,
  getSpecialities,
  prepCrSignUpData,
  uploadFile,
  uploadPdf
} from '../../../api/index'
import {strings} from '../../../common/base'
import {routes} from '../../../common/defaults/routes'
import {defStyles} from '../../../common/defaults/styles'
import AppHeader from '../../../common/Header'
import AppKBAvoidView from '../../../common/KBAvoidView'
import SafeArea from '../../../common/SafeArea'
import AppScrollView from '../../../common/ScrollView'
import AppText from '../../../common/Text'
import {
  showError,
  showInfo,
  showSuccess
} from '../../../common/utils/localNotifications'
import AppView from '../../../common/View'
import AppForm from '../../../components/crSignUp/Form'
import {confirms} from '../../../data/data'
import {pickDoc, upFileParts} from '../../../helper/functions'
import {styles} from './styles'

const SignUp = ({navigation, route}) => {
  let mounted = true

  const options = {
    mediaType: 'photo',
    title: I18n.t('choosePic'),
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
  }

  let type, data, loc
  const _params = route.params
  if (_params) {
    type = _params.type
    data = _params.data
    loc = _params.location
  }

  const dispatch = useDispatch()

  const [crs, setCRs] = useState([])
  const [ctrs, setCtrs] = useState([])
  const [cits, setCits] = useState([])
  const [areas, setAreas] = useState([])
  const [allDays, setAllDays] = useState([])

  const [specs, setSpecs] = useState([])
  const [insces, setInsces] = useState([])
  const [crSpecs, setCrSpecs] = useState([])
  const [crInsces, setCrInsces] = useState([])
  const [crDays, setCrDays] = useState([])

  const [insce, setInsce] = useState('')
  const [insceStatus, setInsceStatus] = useState('')

  const [ctryId, setCtryId] = useState('')
  const [cityId, setCityId] = useState('')

  const [comRegist, setComRegist] = useState('')

  const [checked, setChecked] = useState(false)

  const [isVisibFrom, setIsVisibFrom] = useState(false)
  const [isVisibTo, setIsVisibTo] = useState(false)

  const [visible1, setVisible1] = useState(false)
  const [visible2, setVisible2] = useState(false)

  const [images, setImages] = useState([])

  const [loadCRs, setLoadCRs] = useState(false)
  const [loadCtrys, setLoadCtrys] = useState(false)
  const [loadCitys, setLoadCits] = useState(false)
  const [loadAreas, setLoadAreas] = useState(false)
  const [loadSpecs, setLoadSpecs] = useState(false)
  const [loadInsces, setLoadInsces] = useState(false)
  const [loadComRec, setLoadComRec] = useState(null)

  const [loading, setLoading] = useState(false)
  const [imgLoad, setImgLoad] = useState(false)
  const [loadDays, setLoadDays] = useState(false)

  const [emAvailable, setEmAvailable] = useState('')
  const [phvailable, setPhAvailable] = useState('')
  const [commAvailable, setCommAvailable] = useState('')

  useEffect(() => {
    mounted = true

    if (mounted) {
      fetchCRs()
      fetchCtrs()
      fetchSpecs()
      fetchInsces()
      fetchAllDays()
    }

    return () => (mounted = false)
  }, [])

  useEffect(() => {
    if (ctryId && mounted) {
      setCits([])
      setAreas([])
      fetchCits()
    }

    return () => (mounted = false)
  }, [ctryId])

  useEffect(() => {
    if (cityId && mounted) {
      setAreas([])
      fetchAreas()
    }

    return () => (mounted = false)
  }, [cityId])

  useEffect(() => {
    if (type === 'crInsces') {
      setCrInsces(data)
    } else if (type === 'crSpecs') {
      setCrSpecs(data)
    } else if (type === 'crDays') {
      setCrDays(data)
    }
  }, [crInsces, crSpecs, crDays, type, data])

  const fetchCRs = async () => {
    try {
      setLoadCRs(true)
      const res = await getCenters()
      setCRs(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadCRs(false)
    }
  }

  const fetchCtrs = async () => {
    try {
      setLoadCtrys(true)
      const res = await getCountries()
      setCtrs(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadCtrys(false)
    }
  }

  const fetchCits = async () => {
    try {
      setLoadCits(true)
      const res = await getCities(ctryId)
      setCits(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadCits(false)
    }
  }

  const fetchAreas = async () => {
    try {
      setLoadAreas(true)
      const res = await getAreas(cityId)
      setAreas(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadAreas(false)
    }
  }

  const fetchSpecs = async () => {
    try {
      setLoadSpecs(true)
      const res = await getSpecialities()
      setSpecs(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadSpecs(false)
    }
  }

  const fetchInsces = async () => {
    try {
      setLoadInsces(true)
      const res = await getInsurances()
      setInsces(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadInsces(false)
    }
  }

  const fetchAllDays = async () => {
    try {
      setLoadDays(true)
      const res = await getAllDays()
      setAllDays(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadDays(false)
    }
  }

  const onSubmit = useCallback(
    async (values, setSubmitting) => {
      try {
        setLoading(true)
        if (!insceStatus && !emAvailable && !phvailable && !commAvailable) {
          const _values = prepCrSignUpData({
            ...values,
            commerNm: comRegist.name
          })
          const res = await centerSignUp(_values)
          dispatch(setUserData(res.data))

          const {id, email} = res.data
          showSuccess(res.msg)
          navigation.navigate(routes.otp, {
            id,
            userType: strings.center,
            email
          })
        }
      } catch (error) {
        if (error.type === CONNECTION_ERROR) {
          showError(I18n.t('ui-networkConnectionError'))
        } else {
          showError(error.message)
        }
      } finally {
        setLoading(false)
        setSubmitting
      }
    },
    [loading, images, insceStatus, emAvailable, phvailable, commAvailable]
  )

  const handleImage = useCallback(() => {
    launchImageLibrary(options, response => {
      if (response.didCancel) {
      } else if (response.errorCode) {
      } else {
        const _asset = response?.assets[0]
        if (_asset) {
          const uri = _asset.uri
          ImageResizer.createResizedImage(uri, 500, 500, 'JPEG', 100, 0, null)
            .then(resizImg => {
              // Alert.alert('resized-Url', JSON.stringify(resizImg))
              uploadImg({
                uri: resizImg.uri,
                type: 'image/jpeg',
                name: 'image.jpeg'
              })
            })
            .catch(() => {})
        }
      }
    })
  }, [])

  const uploadImg = async img => {
    try {
      const parts = upFileParts(img)
      if (parts) {
        setImgLoad(true)
        const res = await uploadFile(parts)
        setImages([...images, res.data])
        showSuccess(I18n.t('upSucc'))
      }
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setImgLoad(false)
    }
  }

  const handleFile = useCallback(async () => {
    try {
      setComRegist('')

      let name, type, size
      const file = await pickDoc()

      if (file) {
        name = file.name
        type = file.type
        size = file.size
      }

      if (size <= 1000048) {
        let res

        setLoadComRec(true)

        const parts = upFileParts(file)
        if (type === 'application/pdf') {
          res = await uploadPdf(parts)
        } else if (
          type === 'image/jpeg' ||
          type === 'image/jpg' ||
          type === 'image/png'
        ) {
          res = await uploadFile(parts)
        } else {
          showInfo(I18n.t('notAllowedExt'))
          return
        }
        setComRegist({uri: res.data, name, type})
        showSuccess(I18n.t('upSucc'))
      } else if (size > 1000048) {
        showInfo(I18n.t('1MBSize'))
      }
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadComRec(false)
    }
  }, [])

  const handlePreview = useCallback(() => {
    navigation.navigate(routes.showFile, {
      uri: comRegist.uri,
      type: comRegist.type.includes('image') ? 'image' : 'pdf'
    })
  }, [comRegist, navigation])

  const deleteImg = useCallback(
    value => {
      setImages(images.filter(img => img !== value))
    },
    [images]
  )

  const checkEmail = useCallback(
    async value => {
      try {
        if (10 <= value.length <= 50) {
          const res = await checkUser('email', value, strings.center)
          setEmAvailable(res.data)
        }
      } catch (error) {
        if (error.type === CONNECTION_ERROR) {
          // showError(I18n.t('ui-networkConnectionError'))
        } else {
          // showError(error.message)
        }
      }
    },
    [emAvailable]
  )

  const checkPhoneNo = useCallback(
    async value => {
      try {
        if (9 <= value.toString().length <= 13) {
          const res = await checkUser('mobile', value, strings.center)
          setPhAvailable(res.data)
        }
      } catch (error) {
        if (error.type === CONNECTION_ERROR) {
          // showError(I18n.t('ui-networkConnectionError'))
        } else {
          // showError(error.message)
        }
      }
    },
    [phvailable]
  )

  const checkCommission = useCallback(
    async value => {
      try {
        if (value.length === 11) {
          const res = await checkUser('commission', value, strings.center)
          setCommAvailable(res.data)
        }
      } catch (error) {
        if (error.type === CONNECTION_ERROR) {
          // showError(I18n.t('ui-networkConnectionError'))
        } else {
          // showError(error.message)
        }
      }
    },
    [commAvailable]
  )

  const handleCtryPick = useCallback(id => {
    setCtryId(id)
  }, [])

  const handleCityPick = useCallback(id => {
    setCityId(id)
  }, [])

  const navigToMap = useCallback(() => {
    navigation.navigate(routes.map, {location: loc, _route: routes.crSignUp})
  }, [navigation, loc])

  const navigToInsces = useCallback(() => {
    navigation.navigate(
      routes.crModal,
      {
        data: insces,
        pickedIds: crInsces,
        type: 'crInsces',
        _route: routes.crSignUp
      },
      [insces, crInsces]
    )
  })

  const navigToSpecs = useCallback(() => {
    navigation.navigate(
      routes.crModal,
      {
        data: specs,
        pickedIds: crSpecs,
        type: 'crSpecs',
        _route: routes.crSignUp
      },
      [specs, crSpecs]
    )
  })

  const navigToWDays = useCallback(() => {
    navigation.navigate(
      routes.crModal,
      {
        data: allDays,
        pickedIds: crDays,
        type: 'crDays',
        _route: routes.crSignUp
      },
      [allDays, crDays]
    )
  })

  const navigToUsrTerms = useCallback(() => {
    navigation.navigate(routes.userTerms)
  }, [navigation])

  const checkInsce = useCallback(async value => {
    try {
      if (value.length >= 3) {
        const res = await checkInsurance(value)
        setInsceStatus(res.data)
      }
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    }
  }, [])

  const togglePickFrom = useCallback(() => {
    setIsVisibFrom(!isVisibFrom)
  }, [])

  const togglePickTo = useCallback(() => {
    setIsVisibTo(!isVisibTo)
  }, [])

  const togPassVisib1 = useCallback(() => {
    setVisible1(visible1 => !visible1)
  }, [visible1])

  const togPassVisib2 = useCallback(() => {
    setVisible2(visible2 => !visible2)
  }, [visible2])

  const handleCheck = useCallback(() => {
    setChecked(checked => !checked)
  }, [checked])

  return (
    <SafeArea flex>
      <AppKBAvoidView>
        <AppHeader row rowEnd centerX backBtn />
        <AppScrollView grow>
          <AppView fullHeight defBPad defHPad>
            <AppView stretch colEnd style={styles.ctStyle}>
              <AppText size={defStyles.size20}>{I18n.t('createAcc')}</AppText>
            </AppView>
            <AppView stretch colEnd>
              <AppText>{I18n.t('enterCRDetails')}</AppText>
            </AppView>
            <AppView stretch colEnd style={styles.ctStyle2}>
              <AppText>{I18n.t('areYouCrBr')}</AppText>
            </AppView>
            <AppForm
              {...{confirms}}
              {...{loc}}
              {...{navigToMap}}
              {...{crs}}
              {...{ctrs}}
              {...{cits}}
              {...{areas}}
              {...{crSpecs}}
              {...{crInsces}}
              {...{crDays}}
              {...{navigToInsces}}
              {...{checkInsce}}
              {...{insce}}
              {...{insceStatus}}
              {...{navigToSpecs}}
              {...{loadDays}}
              {...{loadCRs}}
              {...{loadCtrys}}
              {...{loadCitys}}
              {...{loadAreas}}
              {...{loadSpecs}}
              {...{loadInsces}}
              {...{loadComRec}}
              {...{loading}}
              {...{onSubmit}}
              {...{onCtryPick: handleCtryPick}}
              {...{onCityPick: handleCityPick}}
              {...{onUpFile: handleFile}}
              {...{handlePreview}}
              {...{comRegist}}
              {...{handleImage}}
              {...{images}}
              {...{deleteImg}}
              {...{imgLoad}}
              {...{navigToWDays}}
              {...{handleCheck}}
              {...{checked}}
              {...{isVisibFrom}}
              {...{isVisibTo}}
              {...{togPassVisib1}}
              {...{visible1}}
              {...{togPassVisib2}}
              {...{visible2}}
              {...{togglePickFrom}}
              {...{togglePickTo}}
              {...{navigToUsrTerms}}
              {...{emAvailable}}
              {...{checkEmail}}
              {...{phvailable}}
              {...{checkPhoneNo}}
              {...{commAvailable}}
              {...{checkCommission}}
            />
          </AppView>
        </AppScrollView>
      </AppKBAvoidView>
    </SafeArea>
  )
}

export default SignUp
