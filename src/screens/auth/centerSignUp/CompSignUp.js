/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import {useFocusEffect} from '@react-navigation/native'
import I18n from 'i18n-js'
import moment from 'moment'
import React, {memo, useCallback, useEffect, useState} from 'react'
import {Alert, BackHandler} from 'react-native'
import {launchImageLibrary} from 'react-native-image-picker'
import ImageResizer from 'react-native-image-resizer'
import {useDispatch, useSelector} from 'react-redux'
import {refreshList} from '../../../actions/list'
import {setUserData} from '../../../actions/user'
import {CONNECTION_ERROR} from '../../../api/errorTypes'
import {
  addThpToCr,
  getCertifications,
  prepCrCompSignUpData
} from '../../../api/index'
import {uploadFile} from '../../../api/uploadFile/uploadFile'
import {
  AppHeader,
  AppScrollView,
  AppStatusBar,
  AppText,
  AppView,
  defStyles,
  KBAvoidView,
  routes,
  SafeArea,
  showError,
  showSuccess,
  strings
} from '../../../common/base/index'
import AppForm from '../../../components/crSignUp/CompForm'
import {emptyDTsAr} from '../../../data/data'
import {
  getNewCrSchedules,
  getNewCrThpSchedules,
  upFileParts
} from '../../../helper/functions'
import {styles} from './styles'

const Title = memo(props => {
  return (
    <AppView>
      {!props._route ? (
        <AppView>
          <AppView stretch colEnd style={styles.ctStyle}>
            <AppText size={defStyles.size20}>{I18n.t('addNewThp')}</AppText>
          </AppView>
          <AppView stretch colEnd style={styles.ct}>
            <AppText>{I18n.t('enterThpData')}</AppText>
          </AppView>
        </AppView>
      ) : null}
    </AppView>
  )
})

const CompSignUp = ({navigation, route}) => {
  let type, data, _route, thpWDTs, thpData
  const _params = route.params

  if (_params) {
    type = _params.type
    data = _params.data
    _route = _params._route
    thpWDTs = _params.selectWDTs
    thpData = _params.crCompSignUpValues
  }

  const options = {
    mediaType: 'photo',
    title: I18n.t('choosePic'),
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
  }

  const dispatch = useDispatch()

  const crData = useSelector(state => state.user.data)

  const [image, setImage] = useState(null)

  const [certs, setCerts] = useState([])
  const [specs, setSpecs] = useState([])
  const [allDays, setAllDays] = useState([])

  const [crCerts, setCrCerts] = useState([])
  const [crSpecs, setCrSpecs] = useState([])
  const [crDays, setCrDays] = useState([])

  const [thpDays, setThpDays] = useState('')
  const [timesOneText, setTimesOneText] = useState('')

  const [isVisibFrom, setIsVisibFrom] = useState(false)
  const [isVisibTo, setIsVisibTo] = useState(false)

  const [loadCerts, setLoadCerts] = useState(false)
  const [loading, setLoading] = useState(false)
  const [imgLoad, setImgLoad] = useState(false)

  useEffect(() => {
    fetchCerts()
  }, [])

  useEffect(() => {
    if (type === 'crCerts') {
      setCrCerts(data)
    } else if (type === 'crSpecs') {
      setCrSpecs(data)
    } else if (type === 'crDays') {
      setCrDays(data)
    } else if (crData) {
      setSpecs(crData.specialization)
      setAllDays(crData.schedule_days)
    }
  }, [crSpecs, crDays, crData, type, data, thpData])

  useEffect(() => {
    addBHListener()
    return () => rmBHistener()
  }, [])

  useEffect(() => {
    if (thpWDTs) {
      getDaysFormat()
      getTimesFormat()
    }
  }, [thpWDTs])

  const addBHListener = () => {
    BackHandler.addEventListener('hardwareBackPress', () => true)
  }

  const rmBHistener = () => {
    BackHandler.removeEventListener('hardwareBackPress', () => true)
  }

  const fetchCerts = async () => {
    try {
      setLoadCerts(true)
      const res = await getCertifications()
      setCerts(res.data)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoadCerts(false)
    }
  }

  const getDaysFormat = () => {
    const array = []
    const schDays = thpWDTs.schedule_days

    for (let i = 0; i < schDays.length; i++) {
      array.push(emptyDTsAr[schDays[i] - 1])
    }

    setThpDays(array)
  }

  const getTimesFormat = () => {
    let oneText = ''
    const fTimes = thpWDTs.from_times
    const tTimes = thpWDTs.to_times
    const length = fTimes.length

    for (let i = 0; i < fTimes.length; i++) {
      const fTime = moment(fTimes[i], 'HH:mm').locale('ar').format('hh:mm a')
      const tTime = moment(tTimes[i], 'HH:mm').locale('ar').format('hh:mm a')

      const name = `(${I18n.t('from')} ${fTime} ${I18n.t('to')} ${tTime})`
      oneText += i < length - 1 ? `${name}, ` : name
    }

    setTimesOneText(oneText)
  }

  const onSubmit = useCallback(async (values, setSubmitting) => {
    try {
      let _values
      setLoading(true)
      if (_route === routes.editDTsSignUp) {
        _values = prepCrCompSignUpData(values)
      } else if (checkTimeInRangeSignUp(values))
        _values = prepCrCompSignUpData(values)
      const res = await addThpToCr(crData.id, _values)
      dispatch(setUserData(res.data))
      showSuccess(res.msg)
      if (_route === routes.editDTsSignUp) {
        dispatch(refreshList(strings.refCrThps))
        navigation.navigate(routes.registThps)
      } else navigation.navigate(routes.login)
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setLoading(false)
      setSubmitting
    }
  }, [])

  const checkTimeInRangeSignUp = _values => {
    return (
      moment(_values.wTimeTo, 'HH:mm').isSameOrAfter(
        moment(crData?.from_time, 'HH:mm')
      ) &&
      moment(_values.wTimeTo, 'HH:mm').isSameOrBefore(
        moment(crData?.to_time, 'HH:mm')
      )
    )
  }

  const navigToCerts = useCallback(() => {
    navigation.navigate(
      routes.crModal,
      {
        data: certs,
        pickedIds: crCerts,
        type: 'crCerts',
        _route: routes.crCompSignUp
      },
      [certs, crCerts]
    )
  })

  const navigToSpecs = useCallback(() => {
    navigation.navigate(
      routes.crModal,
      {
        data: specs,
        pickedIds: crSpecs,
        type: 'crSpecs',
        _route: routes.crCompSignUp
      },
      [crData, crSpecs]
    )
  })

  const handleWDays = useCallback(() => {
    if (crData.steps === 1) navigToWDays()
    else if (crData.steps === 2) {
      const newCrSchs = getNewCrSchedules(emptyDTsAr, crData.schedule_days)
      const newThpSchs = getNewCrThpSchedules(
        getNewCrSchedules(newCrSchs, getThpDTs()),
        getThpDTs()
      )

      navigToDaysTimes(newCrSchs, newThpSchs)
    }
  }, [crData, crDays, allDays, thpDays, thpWDTs, emptyDTsAr])

  const navigToWDays = () => {
    navigation.navigate(routes.crModal, {
      data: allDays,
      pickedIds: crDays,
      type: 'crDays',
      _route: routes.crCompSignUp
    })
  }

  const navigToDaysTimes = (newCrSchs, newThpSchs) => {
    navigation.navigate(routes.editDTsSignUp, {
      _route: routes.crCompSignUp,
      hTitle: `${I18n.t('adding')} ${I18n.t('thpsSchDays')}`,
      btnTitle: I18n.t('save'),
      crDTs: newCrSchs,
      dayTimes: newThpSchs
    })
  }

  const getThpDTs = () => {
    const array = [...thpDays]

    for (let i = 0; i < thpDays.length; i++) {
      array[i].from_time = thpWDTs.from_times[i]
      array[i].to_time = thpWDTs.to_times[i]
    }

    return array
  }

  const handleImgPick = useCallback(() => {
    launchImageLibrary(options, response => {
      if (response.didCancel) {
      } else if (response.errorCode) {
      } else {
        const _asset = response?.assets[0]
        if (_asset) {
          const uri = _asset.uri
          ImageResizer.createResizedImage(uri, 500, 500, 'JPEG', 100, 0, null)
            .then(resizImg => {
              uploadImg({
                uri: resizImg.uri,
                type: 'image/jpeg',
                name: 'image.jpeg'
              })
            })
            .catch(() => {})
        }
      }
    })
  }, [])

  const uploadImg = async img => {
    try {
      const parts = upFileParts(img)
      if (parts) {
        setImgLoad(true)
        const res = await uploadFile(parts)
        setImage(res.data)
        showSuccess(I18n.t('upSucc'))
      }
    } catch (error) {
      if (error.type === CONNECTION_ERROR) {
        showError(I18n.t('ui-networkConnectionError'))
      } else {
        showError(error.message)
      }
    } finally {
      setImgLoad(false)
    }
  }

  const togglePickFrom = useCallback(() => {
    setIsVisibFrom(isVisibFrom => !isVisibFrom)
  }, [isVisibFrom])

  const togglePickTo = useCallback(() => {
    setIsVisibTo(isVisibTo => !isVisibTo)
  }, [isVisibTo])

  const handleNavig = useCallback(() => {
    if (_route === routes.registThps) navigation.goBack()
    else if (_route === routes.editDTsSignUp)
      navigation.navigate(routes.registThps)
    else navigation.navigate(routes.login)
  }, [_route])

  return (
    <SafeArea flex>
      <AppStatusBar />
      <KBAvoidView>
        <AppHeader
          row
          rowEnd
          centerX
          backBtn
          onBackPress={handleNavig}
          hTitle={_route && `${I18n.t('add')} ${I18n.t('thp')}`}
        />
        <AppScrollView grow>
          <AppView fullHeight defBPad defHPad>
            <Title _route={_route} />
            <AppForm
              {...{_route}}
              {...{loadCerts}}
              {...{loading}}
              {...{isVisibFrom}}
              {...{isVisibTo}}
              {...{crData}}
              {...{thpData}}
              {...{thpWDTs}}
              {...{thpDays}}
              {...{timesOneText}}
              {...{image}}
              {...{upImage: handleImgPick}}
              {...{imgLoad}}
              {...{crCerts}}
              {...{crSpecs}}
              {...{crDays}}
              {...{
                initFTime: crData
                  ? crData.steps === 1
                    ? crData.schedule_days[0].from_time
                    : ''
                  : ''
              }}
              {...{
                initTTime: crData
                  ? crData.steps === 1
                    ? crData.schedule_days[0].to_time
                    : ''
                  : ''
              }}
              {...{navigToCerts}}
              {...{navigToSpecs}}
              {...{handleWDays}}
              {...{handleWTimes: handleWDays}}
              {...{togglePickFrom}}
              {...{togglePickTo}}
              {...{onSubmit}}
            />
          </AppView>
        </AppScrollView>
      </KBAvoidView>
    </SafeArea>
  )
}

export default CompSignUp
