import {StyleSheet} from 'react-native'
import {
  colors,
  responsiveHeight,
  responsiveWidth
} from '../../../common/base/index'

export const styles = StyleSheet.create({
  ctStyle: {
    marginTop: responsiveHeight(2),
    marginBottom: responsiveHeight(1)
  },
  ctStyle2: {
    // marginBottom: responsiveHeight(6),
    marginTop: responsiveHeight(5)
  },
  ct: {
    marginBottom: responsiveHeight(3)
  },

  ct2: {
    marginTop: responsiveHeight(3)
  },
  ct3: {
    marginTop: responsiveHeight(7),
    marginHorizontal: responsiveWidth(4)
    // backgroundColor: 'pink'
  }
  // ctStyle: {
  //   marginTop: responsiveHeight(5)
  // }
})
