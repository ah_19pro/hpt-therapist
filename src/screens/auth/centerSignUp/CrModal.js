/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useCallback, useEffect, useState} from 'react'
// import Search from '../../../components/editProfile/Search'
import {Keyboard} from 'react-native'
import {
  AppAlert,
  AppButton,
  AppHeader,
  AppList,
  AppScrollView,
  AppSpinner,
  AppView,
  SafeArea
} from '../../../common/base/index'
import CheckRow from '../../../components/pickerModal/CheckRow'
import {styles} from './styles'

/*
only left when user filter, i need to show selected items
*/

let array = []

const PickerModal = ({navigation, route}) => {
  const {data, pickedIds, type, _route, _key} = route.params

  const [newData, setNewData] = useState([])
  const [newPickedIds, setNewPickIds] = useState([])
  const [otherId, setOtherId] = useState('')
  const [isVisible, setIsVisible] = useState(false)

  // const [filtered, setFiltered] = useState([])
  const [keyword, setkeyword] = useState('')

  const [loading, setLoading] = useState(false)
  const [loadSpinner, setLoadSpinner] = useState(false)

  useEffect(() => {
    array = []
    if (pickedIds.length > 0) {
      if (_key) isPickedExist()
      else array = [...pickedIds]
      setNewPickIds(pickedIds)
      pickedIds.forEach(item => {
        if (item.id == 113) {
          setOtherId(item.id)
          return
        }
      })
    }
  }, [pickedIds])

  useEffect(() => {
    if (data.length > 0) {
      setNewData(data)
      if (otherId) {
        let dataArray = []
        dataArray = data.map(item => {
          if (item.id == otherId) {
            return {...item, checkedOther: true}
          } else {
            return {...item, checkedOther: false}
          }
        })
        setNewData(dataArray)
      }
    }
  }, [data, otherId])

  const isPickedExist = () => {
    pickedIds.forEach(item => {
      const exist = data.findIndex(_item => _item.parent_id === item.parent_id)
      if (exist !== -1) array = [...array, item]
    })
  }

  const handleChecked = useCallback(
    item => {
      isExist(item)
    },
    [array, newData, newPickedIds, loadSpinner]
  )

  const isExist = _item => {
    if (_item.status) {
      if (_item.id === 113) {
        const newArray = data.map(item => {
          if (item.id === _item.id) {
            return {...item, checkedOther: true}
          } else {
            return {...item, checkedOther: false}
          }
        })
        array = []
        setNewData(newArray)
      }
      addToArray(_item)
    } else {
      // setLoadSpinner(true)
      if (_item.id === 113) {
        const newArray = data.map(item => {
          return {...item, checkedOther: true}
        })
        setNewPickIds([])
        setNewData(newArray)
      }

      const index = array.findIndex(item => {
        const id = item.id //not selected before
        const pId = item.parent_id //not selected before
        const _pId = _item.parent_id //selected before
        const _id = _item.id //selected before

        return id == _id || pId == _id || pId == _pId || id == _pId
      })
      if (index !== -1) {
        //item is already exist
        removeFromArray(index)
      }
      // setLoadSpinner(false)
    }
  }

  const addToArray = item => {
    array.push(item)
    // console.log('item added', JSON.stringify(item))
  }

  const removeFromArray = index => {
    array.splice(index, 1)
    // console.log('item removed', JSON.stringify(x))
  }

  // const handleSearch = useCallback(
  //   value => {
  //     setkeyword(value)
  //     setNewData(
  //       newData.filter(elem =>
  //         elem.name.toLowerCase().includes(value.toLowerCase())
  //       )
  //     )
  //   },
  //   [keyword, newData]
  // )

  // const handleDelete = useCallback(() => {
  //   setkeyword('')
  // }, [keyword])

  const dismissKeyBd = useCallback(() => {
    Keyboard.dismiss()
  }, [])

  const renderCKBx = useCallback(
    ({item}) => (
      <CheckRow
        {...{item}}
        {...{pickedIds: newPickedIds}}
        onCheck={handleChecked}
      />
    ),
    [newPickedIds, array, newData, newPickedIds, loadSpinner]
  )

  const handleData = useCallback(() => {
    // Alert.alert('array', JSON.st ringify(array))
    if (array.length > 0) {
      // Alert.alert('array', JSON.stringify(array))
      setLoading(true)
      removeDuplicates()
      setLoading(false)
      navigation.navigate(_route, {data: array, type})
    } else toggleVisible()
  }, [array, _route, type, toggleVisible, loading])

  const removeDuplicates = () => {
    let _array = []
    const ids = array.map(item => item.id)
    const newIds = [...new Set(ids)]
    for (let i = 0; i < newIds.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (array[j].id === newIds[i]) {
          _array.push(array[j])
          break
        }
      }
    }
    array = _array
  }

  const toggleVisible = useCallback(() => {
    setIsVisible(!isVisible)
  }, [isVisible])

  return (
    <SafeArea flex>
      <AppView flex touchableView opacity={1} onPress={dismissKeyBd}>
        <AppHeader row rowEnd centerX backBtn />
        <AppSpinner loading={loadSpinner} />
        {/* <Search
          {...{keyword}}
          onChange={handleSearch}
          onDelete={handleDelete}
        /> */}
        <AppScrollView grow>
          <AppView fuullHeight>
            <AppList data={newData} renderItem={renderCKBx} />
          </AppView>
        </AppScrollView>
        <AppView defBPad defTPad defHPad>
          <AppButton
            {...{loading}}
            title={I18n.t('save')}
            onPress={handleData}
          />
        </AppView>
        <AppAlert
          {...{isVisible}}
          title={I18n.t('noChoice')}
          actTxtStyle1={styles.ct3}
          actionText1={I18n.t('agree')}
          toggleModal={toggleVisible}
        />
      </AppView>
    </SafeArea>
  )
}

export default PickerModal
