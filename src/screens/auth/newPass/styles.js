import {
  responsiveHeight,
  responsiveWidth
} from '../../../common/utils/responsiveDimensions'
import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
  ctStyle1: {
    marginTop: responsiveHeight(2)
  },
  ctStyle2: {
    marginBottom: responsiveHeight(6),
    marginTop: responsiveHeight(1)
  },
  ctStyle3: {
    marginTop: responsiveHeight(4)
  },
  tStyle: {
    marginEnd: responsiveWidth(1)
  },
  ctStyle4: {
    marginTop: responsiveHeight(6)
  }
})
