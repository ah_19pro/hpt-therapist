/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useCallback, useState} from 'react'
import {CONNECTION_ERROR} from '../../../api/errorTypes'
import {routes} from '../../../common/defaults/routes'
import {defStyles} from '../../../common/defaults/styles'
import {theme} from '../../../common/defaults/theme'
import AppHeader from '../../../common/Header'
import SafeArea from '../../../common/SafeArea'
import AppText from '../../../common/Text'
import {showError, showSuccess} from '../../../common/utils/localNotifications'
import AppView from '../../../common/View'
import AppForm from '../../../components/newPass/Form'
import {styles} from '../login/styles'
import {updatePassword} from '../../../api/auth/updatePass'

const NewPass = ({navigation, route}) => {
  let email, _params
  _params = route.params
  if (_params) email = _params.email

  const [loading, setLoading] = useState(false)

  const [visible1, setVisible1] = useState(false)
  const [visible2, setVisible2] = useState(false)

  const onSubmit = useCallback(
    async (values, setSubmitting) => {
      try {
        const _values = {
          password: values.password,
          password_confirmation: values.passConfirm
        }

        setLoading(true)
        const res = await updatePassword({email, ..._values})
        showSuccess(res.msg)
        navigation.navigate(routes.login)
      } catch (error) {
        if (error.type === CONNECTION_ERROR) {
          showError(I18n.t('ui-networkConnectionError'))
        } else {
          showError(error.message)
        }
      } finally {
        setLoading(false)
        setSubmitting(false)
      }
    },
    [loading, navigation, email]
  )

  const handleNavig = useCallback(() => {
    navigation.navigate(routes.login)
  }, [navigation])

  const togPassVisib1 = useCallback(() => {
    setVisible1(!visible1)
  }, [visible1])

  const togPassVisib2 = useCallback(() => {
    setVisible2(!visible2)
  }, [visible2])

  return (
    <SafeArea flex>
      <AppHeader row rowEnd centerX backBtn onBackPress={handleNavig} />
      <AppView flex style={theme.defPadding}>
        <AppView stretch style={styles.ctStyle1}>
          <AppText size={defStyles.size20}>{I18n.t('createNewPass')}</AppText>
        </AppView>
        <AppView stretch style={styles.ctStyle2}>
          <AppText size={defStyles.size14}>{I18n.t('enterNewPass')}</AppText>
        </AppView>
        <AppForm
          {...{loading}}
          {...{onSubmit}}
          {...{togPassVisib1}}
          {...{visible1}}
          {...{togPassVisib2}}
          {...{visible2}}
        />
      </AppView>
    </SafeArea>
  )
}

export default NewPass
