import {
  responsiveHeight,
  responsiveWidth
} from '../../../common/utils/responsiveDimensions'
import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
  style1: {
    marginTop: responsiveHeight(10)
  },
  tStyle1: {
    marginStart: responsiveWidth(2)
  },
  tStyle2: {
    marginTop: responsiveHeight(1)
  },
  btStyle: {
    width: responsiveWidth(75)
  }
})
