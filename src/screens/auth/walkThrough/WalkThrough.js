/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import I18n from 'i18n-js'
import React, {useCallback, useState} from 'react'
import {AppStatusBar} from '../../../common/base'
import AppButton from '../../../common/button'
import {colors} from '../../../common/defaults/colors'
import {routes} from '../../../common/defaults/routes'
import {defStyles} from '../../../common/defaults/styles'
import {theme} from '../../../common/defaults/theme'
import SafeArea from '../../../common/SafeArea'
import AppText from '../../../common/Text'
import AppView from '../../../common/View'
import Slider from '../../../components/walkthrough/Slider'
import locale from '../../../locales/ar.json'
import {styles} from './styles'

const WalkThrouhg = ({navigation}) => {
  const [index, setIndex] = useState(0)

  // useEffect(() => {
  //   Alert.alert('02-09-2021 10th therapist update')
  // }, [])

  const handleSwiping = useCallback(value => {
    //This is a common unfixed bug in react-native-swiper showing a warning
    //can be temp. fixed with setTimeout
    setTimeout(() => setIndex(value), 0)
  }, [])

  const navigToSelectAcct = () => navigation.navigate(routes.selectAcctType)

  return (
    <SafeArea flex centerX>
      <AppStatusBar />
      <AppView row center style={styles.style1}>
        <AppText bold center size={defStyles.size32} color={colors.secondary}>
          {locale.definitional}
        </AppText>
        <AppText
          bold
          center
          size={defStyles.size32}
          color={colors.darkGrey}
          tStyle={styles.tStyle1}>
          {locale.sentence}
        </AppText>
      </AppView>
      <AppText color={colors.lightGrey} tStyle={styles.tStyle2}>
        {locale.detailedText}
      </AppText>
      <Slider onChange={handleSwiping} />
      <AppButton
        bStyle={[theme.btn, styles.btStyle]}
        title={index !== 2 ? I18n.t('next') : I18n.t('start')}
        onPress={navigToSelectAcct}
      />
    </SafeArea>
  )
}

export default WalkThrouhg
