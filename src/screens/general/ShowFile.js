/* eslint-disable react/prop-types */
import React, {useCallback, useState} from 'react'
import {
  AppHeader,
  AppView,
  SafeArea,
  colors,
  AppIcon,
  iconsType
} from '../../common/base/index'
import PdfViewer from '../../common/PdfViewer'
import {styles} from './styles'
import ImageViewer from 'react-native-image-zoom-viewer'
import {
  windowHeight,
  windowWidth
} from '../../common/utils/responsiveDimensions'

const PdfPresent = ({navigation, route}) => {
  let uri, type
  const _params = route.params
  uri = _params.uri
  type = _params.type

  const [loading, setLoading] = useState(true)

  const closeFile = useCallback(() => {
    navigation.goBack()
  }, [navigation])

  const stopLoad = useCallback(() => {
    setLoading(false)
  }, [loading])

  return (
    <SafeArea flex>
      <AppView flex>
        {/* <AppHeader row rowEnd centerX backBtn /> */}
        {type === 'pdf' ? (
          <PdfViewer
            {...{loading}}
            {...{uri}}
            pdfCtStyle={styles.pdf}
            onLoadComplete={stopLoad}
          />
        ) : (
          <AppView fullHeight stretch>
            <ImageViewer
              imageUrls={[{url: uri}]}
              backgroundColor={colors.darkGrey.color}
            />
          </AppView>
        )}
        <AppView
          absolute
          touchableView
          onPress={closeFile}
          style={styles.close}>
          <AppIcon
            type={iconsType.ant}
            name={'closecircle'}
            color={colors.secondary.color}
          />
        </AppView>
      </AppView>
    </SafeArea>
  )
}

export default PdfPresent
