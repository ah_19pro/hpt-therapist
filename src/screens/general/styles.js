import {StyleSheet} from 'react-native'
import {
  colors,
  responsiveHeight,
  responsiveWidth
} from '../../common/base/index'
import {
  windowHeight,
  windowWidth
} from '../../common/utils/responsiveDimensions'

export const styles = StyleSheet.create({
  close: {
    width: responsiveWidth(7),
    left: responsiveWidth(4.5),
    top: responsiveHeight(4.5)
    // backgroundColor: 'pink'
  },
  pdf: {
    flexGrow: 1,
    width: windowWidth,
    height: windowHeight,
    backgroundColor: colors.primary.color
  }
})
