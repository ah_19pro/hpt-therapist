/* eslint-disable react/prop-types */
import Geolocation from 'react-native-geolocation-service'
import React, {useEffect, useRef, useState, useCallback} from 'react'
import {Alert, StyleSheet} from 'react-native'
import MapView, {PROVIDER_GOOGLE, Animated} from 'react-native-maps'
import {PERMISSIONS, request} from 'react-native-permissions'
import RNSettings from 'react-native-settings'
import I18n from 'i18n-js'
import {styles} from './styles'
import {
  AppHeader,
  AppIcon,
  AppButton,
  AppView,
  iconsType,
  routes,
  colors
} from '../../common/base/index'

let mounted

const MapComponent = ({navigation, route}) => {
  let _location

  const {location: lastLoc, _route} = route.params

  let mapRef = useRef()
  const [currentLoc, setCurrentLoc] = useState(null)
  const [loc, setLoc] = useState(null)

  useEffect(() => {
    mounted = true

    if (mounted) reqPermission()

    return () => (mounted = false)
  }, [])

  const activateGps = useCallback(() => {
    RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).then(
      result => {
        if (result === RNSettings.ENABLED) {
          reqPermission()
        }
      }
    )
  }, [])

  const reqPermission = () => {
    request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(response => {
      if (response === 'granted') {
        getLatLng()
      } else reqPermission()
    })
  }

  const getLatLng = () => {
    Geolocation.getCurrentPosition(
      position => {
        if (lastLoc && mounted) {
          const {latitude: _lat, longitude: _lang} = lastLoc
          const pos = {
            latitude: _lat,
            longitude: _lang,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          }
          setLoc(pos)
          _location = pos
        }
        if (position && mounted) {
          const {latitude, longitude} = position.coords
          const pos = {
            latitude,
            longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          }
          if (!lastLoc) setLoc(pos)
          _location = pos
          setCurrentLoc(pos)
        }
      },
      error => {
        console.log('🚀 ~ file: Map.js ~ line 116 ~ getLatLng ~ error', error)
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000}
    )
  }

  const renderMap = () => {
    return (
      <Animated
        style={{...StyleSheet.absoluteFillObject}}
        provider={PROVIDER_GOOGLE}
        region={loc}
        ref={mapRef}
        onRegionChangeComplete={handleChange}
      />
    )
  }

  const selectPos = useCallback(() => {
    if (_route === routes.thpSignUpSt1)
      navigation.navigate(routes.thpSignUpSt1, {location: _location})
    else if (_route === routes.crSignUp)
      navigation.navigate(routes.crSignUp, {location: _location})
    else if (_route === routes.editThpProf)
      navigation.navigate(routes.editThpProf, {
        location: _location,
        status: true
      })
    else if (_route === routes.editCrProf)
      navigation.navigate(routes.editCrProf, {
        location: _location,
        status: true
      })
  }, [_location, _route])

  const goToCurrentPos = useCallback(() => {
    setLoc({...currentLoc})
  }, [loc, currentLoc])

  const handleChange = useCallback(
    region => {
      _location = {...region}
    },
    [_location]
  )

  return (
    <AppView flex stretch>
      {loc ? (
        renderMap()
      ) : (
        <AppView flex center>
          <AppButton title={I18n.t('activateGPS')} onPress={activateGps} />
        </AppView>
      )}
      <AppHeader backBtn ctStyle={styles.ctStyle} />
      {loc ? (
        <AppView absolute style={styles.marker}>
          <AppIcon
            name='map-pin'
            type={iconsType.fontAwesome5}
            color={colors.red.color}
          />
        </AppView>
      ) : null}
      {loc ? (
        <AppView absolute stretch centerY style={styles.ct}>
          <AppView
            touchableView
            style={styles.location}
            onPress={goToCurrentPos}>
            <AppIcon
              type={iconsType.material}
              name='my-location'
              size={35}
              color={colors.black.color}
            />
          </AppView>
          <AppView absolute style={styles.ct2}>
            <AppButton
              bStyle={styles.btn}
              onPress={selectPos}
              title={
                _route === routes.editThpProf || _route === routes.editCrProf
                  ? I18n.t('updateLoc')
                  : I18n.t('selectLoc')
              }
            />
          </AppView>
        </AppView>
      ) : null}
    </AppView>
  )
}

export default MapComponent

{
  /* <Marker coordinate={loc}>
          <AppView center style={defStyles.absolute}>
            <AppIcon
              name="map-pin"
              type={iconsType.fontAwesome5}
              color={'red'}
            />
          </AppView>
        </Marker> */
}

// const handlePress = useCallback(
//   e => {
//     const {latitude, longitude} = e.nativeEvent.coordinate;
//     setLoc({
//       latitude,
//       longitude,
//       latitudeDelta: 0.0922,
//       longitudeDelta: 0.0421,
//     });
//   },
//   [loc],
// );
