import {StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../../common/base'
import {colors} from '../../common/defaults/colors'

export const styles = StyleSheet.create({
  ctStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    // zIndex: 1,
    marginHorizontal: 0,
    paddingHorizontal: '7%',
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: colors.transp.color
  },
  ct2: {
    left: '29%',
    bottom: responsiveHeight(2)
  },
  location: {
    width: responsiveWidth(10),
    marginBottom: responsiveHeight(3),
    marginStart: responsiveWidth(4)
  },
  ct: {
    bottom: 0
  },
  btn: {
    backgroundColor: colors.secondary.color,
    borderRadius: 8,
    height: responsiveHeight(7.5),
    width: responsiveWidth(40)
  },
  marker: {
    top: '50%',
    left: '50%'
  }
})
